/************************************************************************\
 * Magic Square solves magic squares.                                   *
 * Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>           *
 *                                                                      *
 * This file is part of Magic Square.                                   *
 *                                                                      *
 * Magic Square is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * Magic Square is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with Magic Square.  If not, see                                *
 * <https://www.gnu.org/licenses/>.                                     *
\************************************************************************/

/* write.c -- functions for outputting magic squares */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <endian.h>
#include <assert.h>
#ifdef HAVE_LIBGMP_ALL
# if defined HAVE_LOCALE_H
#  include <locale.h>
# elif defined HAVE_LOCALECONV
/* Don't use localeconv() even though we have it because we don't have
   the header file. */
#  undef HAVE_LOCALECONV
# endif /* !HAVE_LOCALE_H && HAVE_LOCALECONV */
# include <ctype.h>
# include <gmp.h>
#endif
#include <mu/safe.h>

#include "write.h"
#include "parse.h" /* For HLINE, VLINE, and JOINT */
#include "square.h"

/* The width to wrap at if the square is smaller than this */
#define MIN_WRAP_WIDTH 70

#ifdef HAVE_LIBGMP_ALL
/* A list of `mpz_t's. */
typedef struct mpz_list {
  struct mpz_list *next;
  mpz_t num;
} zlist_t;
#endif

/* A list of `int's. */
typedef struct int_list {
  struct int_list *next;
  int num;
} ilist_t;

/* Static helper functions */
static int write_machine_cellval(cellval_t, FILE *);

#ifdef HAVE_LIBGMP_ALL
static char *get_optimal_str_mpq(const mpq_t);
static size_t get_optimal_str_mpq_arg(char **, size_t, const mpq_t,
				      int, int, int, int);
static size_t get_float_str_mpq(char **, size_t, const mpq_t,
				int, int, int, int);
static const char *get_prefix(int);
static int is_terminating_in_bases(const mpq_t, const int *, size_t);
static int get_prime_factors_mpz(mpz_t **, size_t *, const mpz_t);
static int get_prime_factors(int **, size_t *, int);
#endif /* HAVE_LIBGMP_ALL */

static size_t write_human_separator(size_t, size_t, FILE *);
static size_t write_human_other_nums(cellval_t *, size_t, size_t, FILE *);

static int cellval_compare(const void *, const void *);

/* Write a magic square in a machine-readable format */
size_t write_machine(const square_t *square, FILE *file) {
  size_t description_size,
    description_size_be, square_size_be, nums_size_be;
  const uint32_t magic_number = htobe32(MAGIC_NUMBER);
  unsigned char *condensed_mutable_cells;
  size_t condensed_mutable_cells_size;
  size_t index = 0;
  char subindex = 0;

  /* Get the description size */
  description_size = square->description ? strlen(square->description) : 0;

  /* Convert endianness */
  description_size_be = htobe64(description_size);
  square_size_be = htobe64(square->size);
  nums_size_be = htobe64(square->nums_size);

  /* Write all the sizes and stuff */
  if (!(/* Write the magic number */
	fwrite(&magic_number, sizeof(magic_number), 1, file) &&

	/* Write the file version number */
	putc(FILE_VERSION, file) != EOF &&

	/* Indicate whether this file has regular C types or GMP
	   types. */
#ifdef HAVE_LIBGMP_ALL
	putc(1, file) != EOF &&
#else
	putc(0, file) != EOF &&
#endif

	/* Write the square size */
	fwrite(&square_size_be, sizeof(square_size_be), 1, file) &&

	/* And the other numbers size */
	fwrite(&nums_size_be, sizeof(nums_size_be), 1, file) &&

	/* Write the description size */
	fwrite(&description_size_be, sizeof(description_size_be), 1, file))) {
    return 0; /* Zero to indicate an error even though we might have
		 written some stuff */
  }

  /* Get the cells in a condensed form for efficient writing (1 bit
     for each cell, padded with zeros if mutable_cells_size^2 is not
     divisible by 8). */
  condensed_mutable_cells_size =
    ((square->size * square->size) + 7) / 8;
  condensed_mutable_cells =
    mu_xcalloc(condensed_mutable_cells_size,
	       sizeof(*condensed_mutable_cells));

  for (size_t x = 0; x < square->size; x++) {
    for (size_t y = 0; y < square->size; y++) {
      /* Get the condensed form. The !! is just to make sure it is
	 only 0 or 1. */
      condensed_mutable_cells[index] |=
	!!square->cells[x][y].mutable << subindex;

      if (++subindex > 7) {
	subindex = 0;
	index++;

	assert(index < condensed_mutable_cells_size ||
	       (index == condensed_mutable_cells_size && !subindex));
      }
    }
  }

  assert(index == condensed_mutable_cells_size - !!subindex);
  assert(subindex == (square->size * square->size) % 8);

  /* Write the condensed form. */
  if (fwrite(condensed_mutable_cells, sizeof(*condensed_mutable_cells),
	     condensed_mutable_cells_size, file) !=
      condensed_mutable_cells_size) {
    free(condensed_mutable_cells);
    return 0;
  }

  free(condensed_mutable_cells);

  /* Now write the cells */
  for (size_t x = 0; x < square->size; x++) {
    for (size_t y = 0; y < square->size; y++) {
      if (!square->cells[x][y].mutable &&
	  write_machine_cellval(square->cells[x][y].val, file)) {
	return 0;
      }
    }
  }

  /* Write the other valid numbers */
  for (size_t i = 0; i < square->nums_size; i++) {
    if (write_machine_cellval(square->nums[i], file))
      return 0;
  }

  /* And the description */
  if (fwrite(square->description, sizeof(*(square->description)),
	     description_size, file) != description_size) {
    return 0;
  }

  /* Return the bytes written */
  return
    sizeof(magic_number) + sizeof(square->size) + sizeof(description_size) +
    (sizeof(**(square->cells)) * square->size * square->size) +
    (sizeof(*(square->nums)) * square->nums_size) +
    (sizeof(*(square->description)) * description_size);
}

/* Write a magic square in a human-readable format, returning 0 on
   error, number of bytes written on success. */
size_t write_human(const square_t *square, FILE *file) {
  struct {
    char *str;
    int length, index;
  } **outstrings; /* String representations of the numbers */
  size_t max_length = 0; /* The longest string in `outstrings' */
  size_t cell_width, cell_height; /* The output cell size */
  size_t bytes_written = 0; /* How many bytes we have written */
  size_t ret;

  /* First print the description if there is one */
  if (square->description) {
    int iret = fprintf(file, "%s\n\n", square->description);

    if (iret < 0)
      return 0;

    bytes_written += iret;
  }

  outstrings = mu_xmalloc(sizeof(*outstrings) * square->size);

  for (size_t x = 0; x < square->size; x++) {
    outstrings[x] =
      mu_xmalloc(sizeof(*(outstrings[x])) * square->size);

    for (size_t y = 0; y < square->size; y++) {
      if (square->cells[x][y].mutable) {
	/* Don't output anything for this one */
	outstrings[x][y].str = NULL;
	outstrings[x][y].length = 0;
      }
      else {
#ifdef HAVE_LIBGMP_ALL
	outstrings[x][y].str =
	  get_optimal_str_mpq(cellval_val(square->cells[x][y].val));
	if (!outstrings[x][y].str) {
#else /* !HAVE_LIBGMP_ALL */
	switch (square->cells[x][y].val.type) {
	case INT:
	  outstrings[x][y].length = asprintf(&(outstrings[x][y].str), "%lld",
					     square->cells[x][y].val.i);
	  break;
	case FLOAT:
	  outstrings[x][y].length = asprintf(&(outstrings[x][y].str), "%Lg",
					     square->cells[x][y].val.f);
	  break;
	default:
	  /* Invalid type! */
#endif /* !HAVE_LIBGMP_ALL */
	  for (size_t i = 0; i <= x; i++) {
	    for (size_t j = 0; j < y; j++) {
	      if (outstrings[i][j].str)
		free(outstrings[i][j].str);
	    }

	    free(outstrings[i]);
	  }

	  free(outstrings);

	  errno = EINVAL;
	  return 0;
	}
#if 0
	} /* Keep emacs happy. */
#endif

#ifdef HAVE_LIBGMP_ALL
	outstrings[x][y].length = strlen(outstrings[x][y].str);
#endif

	if (outstrings[x][y].length > max_length)
	  max_length = outstrings[x][y].length;
      }

      outstrings[x][y].index = 0;
    }//							     +----+
  }  //						    +--+     |	  |
     //						    |  |     |	  |
  /* Use a cell ratio of 2:1 since that looks best (+--+ and +----+
     for example). */
  for (cell_height = 1;
       cell_height * (cell_width = cell_height * 2) < max_length;
       cell_height++) {
    /* Check for overflow */
    if (cell_height == SIZE_MAX / 2) {
      errno = EOVERFLOW;
      goto error;
    }
  }

  /* Write the first separator */
  if (!(ret = write_human_separator(cell_width, square->size, file)))
    goto error;

  bytes_written += ret;

  /* Write the rest */
  for (size_t square_y = 0; square_y < square->size; square_y++) {
    for (size_t cell_y = 0; cell_y < cell_height; cell_y++) {
      if (putc(VLINE, file) == EOF)
	goto error;

      bytes_written++;

      for (size_t square_x = 0; square_x < square->size; square_x++) {
	for (size_t cell_x = 0; cell_x < cell_width; cell_x++) {
	  if (outstrings[square_x][square_y].index <
	      outstrings[square_x][square_y].length) {
	    /* There's still stuff left to print in this string */
	    if (putc(outstrings[square_x][square_y].str
		     [outstrings[square_x][square_y].index++],
		     file) == EOF) {
	      goto error;
	    }

	    bytes_written++;
	  }
	  else {
	    /* We're done printing this string, just fill the rest
	       with spaces */
	    if (putc(' ', file) == EOF)
	      goto error;

	    bytes_written++;
	  }
	}

	/* Add the seperator */
	if (putc(VLINE, file) == EOF)
	  goto error;

	bytes_written++;
      }

      /* Start a new line */
      if (putc('\n', file) == EOF)
	goto error;

      bytes_written++;
    }

    /* Print the separator */
    if (!(ret = write_human_separator(cell_width, square->size, file)))
      goto error;

    bytes_written += ret;
  }

  /* Write the other numbers if there are any */
  if (square->nums_size) {
    size_t wrap; /* Where to wrap the line */

    if (putc('\n', file) == EOF)
      goto error;

    bytes_written++;

    /* Wrap at the width of the square or at MIN_WRAP_WIDTH if the
       square is smaller than that */
    if ((wrap = square->size * (cell_width + 1) + 1) < MIN_WRAP_WIDTH)
      wrap = MIN_WRAP_WIDTH;

    ret = write_human_other_nums(square->nums, square->nums_size, wrap, file);

    if (!ret)
      goto error;

    bytes_written += ret;
  }

  goto success;

 error:
  bytes_written = 0;

 success:
  for (size_t x = 0; x < square->size; x++) {
    for (size_t y = 0; y < square->size; y++) {
      if (outstrings[x][y].str)
	free(outstrings[x][y].str);
    }

    free(outstrings[x]);
  }

  free(outstrings);

  return bytes_written;
}

/***************************\
|* Static helper functions *|
\***************************/

/* Write a single `cellval_t'; returns 0 on success, nonzero on
   error. */
static int write_machine_cellval(cellval_t cellval, FILE *file) {
#ifdef HAVE_LIBGMP_ALL
  size_t count, count_be;
  void *data;

  /* Ensure the cellval is canonicalized. */
  mpq_canonicalize(cellval_val(cellval));

  /* Check if the cellval is negative and write that information to
     the file. */
  if (putc(!(mpq_sgn(cellval_val(cellval)) + 1), file) == EOF)
    return 1;

  /* First get the numerator. The first `1' after `&count' is the word
     order. `1' means most significant word first (big endian). The
     second `1' is the word size. We set this to one byte. The third
     `1' is the endianness within each word. `1' means most
     significant byte first (big endian), but it doesn't actually
     matter since our word size is only one byte. The `0' is the
     number of most significant bits of each word that will be unused
     and set to zero. We want to use all bits, hence the `0'. */
  data =
    mpz_export(NULL, &count, 1, 1, 1, 0, mpq_numref(cellval_val(cellval)));
  if (!data)
    return 1;

  /* Convert the count to big endian. */
  count_be = htobe64(count);

  /* Write the count and the numerator itself. */
  if (!fwrite(&count_be, sizeof(count_be), 1, file) ||
      fwrite(data, 1, count, file) != count) {
    free(data);
    return 1;
  }

  free(data);

  /* Repeat for the denominator. */
  data =
    mpz_export(NULL, &count, 1, 1, 1, 0, mpq_denref(cellval_val(cellval)));
  if (!data)
    return 1;
  count_be = htobe64(count);
  if (!fwrite(&count_be, sizeof(count_be), 1, file) ||
      fwrite(data, 1, count, file) != count) {
    free(data);
    return 1;
  }
  free(data);
#else /* !HAVE_LIBGMP_ALL */
  if (putc(cellval.type, file) == EOF)
    return 1;

  switch (cellval.type) {
    cellvali_t cellvali;

  case INT:
    cellvali = htobe64(cellval.i);

    if (!fwrite(&cellvali, sizeof(cellvali), 1, file))
      return 1;

    break;
  case FLOAT:
    /* TODO: Make this portable. I could not figure out how to
       portably write long doubles (or any floating point numbers,
       for that matter) to a binary file. */

    if (!fwrite(&(cellval.f),
		sizeof(cellval.f), 1, file))
      return 1;

    break;
  default:
    return 1;
  }
#endif /* !HAVE_LIBGMP_ALL */

  return 0;
}

#ifdef HAVE_LIBGMP_ALL
/* A convenience wrapper for get_optimal_str_mpq_args(). Returns the
   resultant dynamically-allocated string on success or NULL on error,
   in which case `errno' will be set to indicate the error. */
static char *get_optimal_str_mpq(const mpq_t number) {
  char *res;
  if (!get_optimal_str_mpq_arg(&res, 0, number, 1, 1, 4, 0))
    return NULL;
  return res;
}

/* Get the optimal representation (floating point if possible,
   fraction if not) into `*res' from an `mpq_t' rational
   number. Arguments are as for get_float_str_mpq() (see below).
   Returns the length of `*res' (not including the terminating null
   byte) on success or zero on error, in which case `errno' will be
   set to indicate the error. */
static size_t get_optimal_str_mpq_arg(char **res, size_t max_size,
				      const mpq_t number,
				      int base, int prefix,
				      int exp_thresh, int exp_mode) {
  const char *prefix_str;
  char *str;
  size_t str_size, prefix_str_size;

  str_size = get_float_str_mpq(res, max_size, number, base,
			       prefix, exp_thresh, exp_mode);
  if (str_size || errno != EOVERFLOW)
    return str_size;

  /* It can't be exactly represented as a floating point string, so we
     need to represent it as a fraction. */
  if (base == 1 || base == -1)
    base = 10;			/* Default to decimal */
  prefix_str = prefix ? get_prefix(base) : "";
  prefix_str_size = strlen(prefix_str);
  str = mpq_get_str(NULL, base, number);
  str_size = prefix_str_size + strlen(str);
  if (!max_size)
    *res =
      mu_xmalloc(sizeof(**res) * (str_size + 1)); /* +1 for '\0' */
  else if (str_size > max_size) {
    free(str);
    errno = EINVAL;
    return 0;
  }

  /* Copy over the string. */
  memcpy(*res, prefix_str, prefix_str_size);
  memcpy(*res + prefix_str_size, str, str_size - prefix_str_size);
  (*res)[str_size] = '\0';
  free(str);

  /* We're done. */
  return str_size;
}

/* Get a floating point string (such as "2.564e2") into `*res' from an
   `mpq_t' rational number. `*res' should be a string of at least
   `max_size' length, and we will write at most `max_size' bytes into
   it; or, if `max_size' is zero, `*res' will be allocated with
   mu_xmalloc(). `number_arg' is the rational number to
   convert. `base' is the base to convert to and must be from 1 to 62
   inclusive or -1 to -36 inclusive. If `base' is negative, capital
   letters are used. Otherwise, if `base' is between 2 and 36
   inclusive, lowercase letters are used. If `base' is between 37 and
   62 inclusive, uppercase letters are used for lower digits and
   lowercase for higher digits. If `base' is 1 or -1, the first base
   that will not produce an infinitely repeating number will be
   used. Bases are checked in this order: 10, 16, 8, 2, and then from
   2-62. If, however, `prefix' is nonzero, ONLY 10, 16, 8, and 2 will
   be checked. If `base' is +/-(1, 2, 8, or 16), `prefix' can be
   nonzero, in which case a prefix of "0b", "0", or "0x" will be
   prepended to the string ("0B", "0", or "0X" if base is
   negative). If base is not +/-(1, 2, 8, or 16), `prefix' is silently
   ignored. `exp_thresh' is the threshold to use exponent
   notation. I.e., how many zeros (in base `base') are required before
   using exponent notation. If `exp_thresh' is zero, always use
   exponent notation. If it is negative, never use exponent
   notation. `exp_mode' can either be `e', `p', or `@' (case
   insensitive). `e' and `@' mean the exponent base is the same as
   `base'. The difference is that `e' is only valid if `base' <= 10,
   whereas `@' is always valid. `p' is only valid in bases 2 and 16,
   and it means an exponent base of 2. If `exp_mode' is 0, `e' is used
   for bases <= 10 except 2, 'p' is used for bases 2 and 16, and `@'
   is used for everything else. An `exp_mode' of 1 is the same as 0
   except that the character used will be capital. Any other value is
   an error unless `exp_thresh' is negative.

   If `number_arg' cannot fit into a string of length `max_size', zero
   is returned and the contents of `*res' are undefined. If `max_size'
   is zero, but `number_arg' cannot fit into a finite string, zero is
   returned and `*res' will not be allocated. In both of these cases,
   `errno' will be set to EOVERFLOW. If the arguments are invalid,
   zero is returned and `errno' is set to EINVAL. If `res' is NULL,
   zero is returned and `errno' is set to EFAULT. On success, the
   length of the resulting string (in `*res'), not including the
   terminating null byte is returned. */
static size_t get_float_str_mpq(char **res, size_t max_size,
				const mpq_t number_arg, int base,
				int prefix, int exp_thresh,
				int exp_mode) {
  mpq_t number;
  mpz_t quotient, remainder, dividend;
  int exp_base = 0;
  char decimal_point;
  clist_t *char_list, *char_list_beg, *decimal_pos;
  const char *prefix_str = NULL;
  char *exp_power_str = NULL;
  size_t prefix_str_size = 0, str_size = 0, exp_power = 0, index = 0;
  int exp_power_str_size = 0;
  char is_neg = 0;

  /* Make sure our arguments are valid. */
  if (res == NULL) {
    errno = EFAULT;
    return 0;
  }
  if (base == 0 || base > 62 || base < -36) {
    errno = EINVAL;
    return 0;
  }

  mpq_init(number);

  /* Check whether the number is negative. */
  if (mpz_cmp_ui(mpq_numref(number_arg), 0) < 0) {
    is_neg = 1;

    /* We are going to add a '-' to the beginning of the output
       string. */
    str_size++;

    mpq_neg(number, number_arg);
  }
  else {
    mpq_set(number, number_arg);
  }

  if (base == 1 || base == -1) {
    const int bases[] =
      { 10, 16,  8,  2,  3,  4,  5,  6,  7,  9, 11, 12, 13,
	14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
	28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
	54, 55, 56, 57, 58, 59, 60, 61, 62 };

    /* Use the first base which won't produce an infinitely repeating
       decimal. First check 10, 16, 8, and 2, and then check the other
       bases if `prefix' is zero. */
    base *=			/* Preserve the sign of the base. */
      is_terminating_in_bases(number, bases, prefix ? 4 :
			      (sizeof(bases) / sizeof(*bases)));

    assert(base != -1);
    assert(base !=  1);

    if (base == 0) {
      /* It is not terminating in any of the bases. */
      mpq_clear(number);
      errno = EOVERFLOW;
      return 0;
    }
  }
  else if (!is_terminating_in_bases(number, &base, 1)) {
    mpq_clear(number);
    errno = EOVERFLOW;
    return 0;
  }

  if (exp_thresh >= 0) {
    if (exp_mode == 0 || exp_mode == 1) {
      /* Determine the exponent mode based on `base' (no pun
	 intended). */
      if (base == 2 || base == 16)
	exp_mode = exp_mode ? 'P' : 'p';
      else if (base <= 10)
	exp_mode = exp_mode ? 'E' : 'e';
      else
	exp_mode = '@';
    }

    /* Get the exponent base. */
    switch (tolower(exp_mode)) {
    case 'e':
      /* An 'e' exponent is only valid in bases up to 10. */
      if (base > 10) {
	mpq_clear(number);
	errno = EINVAL;
	return 0;
      }

      exp_base = base;

      break;
    case 'p':
      /* A 'p' exponent is only valid in bases 2 and 16. */
      if (base != 2 && base != 16) {
	mpq_clear(number);
	errno = EINVAL;
	return 0;
      }

      exp_base = 2;

      break;
    case '@':
      /* An '@' exponent is always valid. */
      exp_base = base;

      break;
    default:
      mpq_clear(number);
      errno = EINVAL;
      return 0;
    }
  }

  if (exp_thresh > 0) {
    /* The test below doesn't work for 0, so we have to check for it
       here. */
    if (mpq_cmp_ui(number, 0, 1) == 0)
      exp_base = 0;
    else {
      mpq_t test_number;

      /* Make a copy since our test is destructive. */
      mpq_init(test_number);
      mpq_set(test_number, number);

      /* Determine whether we should actually use exponent notation or
	 not. */
      for (int i = 0; i < exp_thresh; i++) {
	/* Chop off a zero. */
	mpz_mul_si(mpq_denref(test_number), mpq_denref(test_number), base);
	mpq_canonicalize(test_number);

	if (mpz_cmp_ui(mpq_denref(test_number), 1) != 0) {
	  /* The denominator isn't 1 which means we ran out of
	     zeros. So don't use exponent notation (we set `exp_base'
	     to zero to indicate that). */
	  exp_base = 0;
	  break;
	}
      }

      /* We are done with this. */
      mpq_clear(test_number);
    }
  }

  if (prefix) {
    /* Add an appropriate prefix. */
    prefix_str = get_prefix(base);
    prefix_str_size = strlen(prefix_str);

    str_size += prefix_str_size;

    /* >= because if it is equal to `max_size', we won't have room for
        the number no matter how small it is. */
    if (max_size && str_size >= max_size) {
      mpq_clear(number);
      errno = EOVERFLOW;
      return 0;
    }
  }


  char_list		= mu_xmalloc(sizeof(*char_list));
  char_list_beg		= char_list; /* So we can quickly get back. */
  char_list->prev	= NULL; /* Remember where the beginning is. */
  decimal_pos		= NULL;

# ifdef HAVE_LOCALECONV
  /* Get the decimal point for the current locale. */
  decimal_point = *(localeconv()->decimal_point);
# else
  decimal_point = '.';
# endif

  mpz_inits(quotient, remainder, dividend, NULL);
  mpz_set(remainder, mpq_numref(number));
  mpz_set(dividend, mpq_denref(number));

  /* Now we perform long division just like you learned in
     school. (Actually, it's a bit different since this version is
     easier for machines.) */
  while (1) {
    char *tmp_str;
    size_t tmp_str_len;

    /* 1.1.  Check how many times the dividend will go into the
	     divisor. */
    mpz_tdiv_qr(quotient, remainder, remainder, dividend);

    /* 1.2.  Write down the answer. */
    tmp_str	= mpz_get_str(NULL, base, quotient);
    tmp_str_len	= strlen(tmp_str);

    str_size += tmp_str_len;

    /* Check that the string isn't too long. */
    if (max_size && str_size > max_size) {
      while (char_list->prev) {
	char_list = char_list->prev;
	free(char_list->next);
      }
      free(char_list);
      mpq_clear(number);
      mpz_clears(quotient, remainder, dividend, NULL);

      errno = EOVERFLOW;
      return 0;
    }

    for (size_t i = 0; i < tmp_str_len; i++) {
      char_list->c		= tmp_str[i];
      char_list->next		=
	mu_xmalloc(sizeof(*(char_list->next)));
      char_list->next->prev	= char_list;
      char_list			= char_list->next;
    }

    free(tmp_str);

    /* 2.1.  Get the remainder... oh wait! We already did. */

    if (mpz_cmp_ui(remainder, 0) == 0) {
      /* No remainder. We're done! */
      break;
    }
    else {
      /* 2.2.  There is a remainder, so write a decimal point unless
	       we did already. */
      if (!decimal_pos) {
	/* Remember that we wrote a decimal point and where it is. */
	decimal_pos = char_list;

	/* Remember the power (in base `base') to raise the number to
	   when we move the decimal point (if we do move it). */
	if (exp_base)
	  exp_power = str_size - prefix_str_size - 1;

	/* Write the decimal point. */
	char_list->c		= decimal_point;
	char_list->next		=
	  mu_xmalloc(sizeof(*(char_list->next)));
	char_list->next->prev	= char_list;
	char_list		= char_list->next;
	str_size++;

	/* Check that the string isn't too long. */
	if (max_size && str_size > max_size) {
	  while (char_list->prev) {
	    char_list = char_list->prev;
	    free(char_list->next);
	  }
	  free(char_list);
	  mpq_clear(number);
	  mpz_clears(quotient, remainder, dividend, NULL);

	  errno = EOVERFLOW;
	  return 0;
	}
      }

      /* 3.  Pull down a zero. */
      mpz_mul_si(remainder, remainder, base);
    }

    /* 4.  Repeat! (Note that we know this won't go on forever because
	   we checked earlier.) */
  }

  mpq_clear(number);
  mpz_clears(quotient, remainder, dividend, NULL);

  if (exp_base) {
    assert(!decimal_pos == !exp_power);

    if (decimal_pos) {
      /* Cut the decimal from where it is. */
      decimal_pos->prev->next	= decimal_pos->next;
      decimal_pos->next->prev	= decimal_pos->prev;

      /* And paste it right after the beginning. */
      decimal_pos->prev		= char_list_beg;
      decimal_pos->next		= char_list_beg->next;
      decimal_pos->prev->next	= decimal_pos;
      decimal_pos->next->prev	= decimal_pos;
    }
    else {
      exp_power = str_size - prefix_str_size - 1;

      /* Add in the decimal point. */
      decimal_pos		=
	mu_xmalloc(sizeof(*(char_list_beg->next)));
      decimal_pos->prev		= char_list_beg;
      decimal_pos->next		= char_list_beg->next;
      decimal_pos->prev->next	= decimal_pos;
      decimal_pos->next->prev	= decimal_pos;
      decimal_pos->c		= decimal_point;
      str_size++;
    }

    /* Remove redundant zeros. */
    while (char_list->prev->c == '0' && char_list->prev != decimal_pos) {
      char_list = char_list->prev;
      free(char_list->next);
      str_size--;
    }
    if (char_list->prev == decimal_pos) {
      /* We can remove the decimal point as well. */
      char_list->prev = char_list->prev->prev;
      char_list->prev->next = char_list;
      free(decimal_pos);
      decimal_pos = NULL;
      str_size--;
    }
    char_list->next = NULL;

    if (exp_base != base) {
      assert(exp_base == 2);
      assert(base == 16);

      /* We need to convert from an exponent base of 16 to 2. Since
	 16^n == 2^(4*n), we can just do this: */
      exp_power *= 4;
    }

    /* Get the exponent power as a string. For some brain-damaged
       reason, the convention is to always use base ten for the
       exponent, no matter what the base is. Nevertheless, we must
       follow the convention. */
    exp_power_str_size =
      mu_xasprintf(&exp_power_str, "%zu", exp_power);

    /* We are going to add that string and also a character indicating
       the power ([eEpP@]). */
    str_size += exp_power_str_size + 1;

    /* Check that the string isn't too long. */
    if (max_size && str_size > max_size) {
      while (char_list->prev) {
	char_list = char_list->prev;
	free(char_list->next);
      }
      free(char_list);

      errno = EOVERFLOW;
      return 0;
    }
  }

  /* Allocate the string (`str_size' does not include the '\0'). */
  *res = mu_xmalloc(sizeof(**res) * (str_size + 1));

  /* Add a '-' sign if the number is negative. */
  if (is_neg)
    (*res)[index++] = '-';

  /* Add the prefix if any. */
  for (unsigned char i = 0; i < prefix_str_size; i++)
    (*res)[index++] = prefix_str[i];

  /* Copy the list to the string. */
  char_list->next = NULL;
  char_list = char_list_beg;
  while (char_list->next) {
    (*res)[index++] = char_list->c;

    char_list = char_list->next;
    free(char_list->prev);
  }
  free(char_list);

  if (exp_base) {
    /* Add the exponent. */
    (*res)[index++] = exp_mode;
    for (size_t i = 0; i < exp_power_str_size; i++)
      (*res)[index++] = exp_power_str[i];

    free(exp_power_str);
  }

  /* Finish off the string. */
  (*res)[index] = '\0';

  assert(strlen(*res) == index);
  assert(index == str_size);

  /* Whew! We're finally done! */
  return str_size;
}

/* Get the prefix for `base', or "" if there is none. Never returns
   NULL. */
static const char *get_prefix(int base) {
  switch (base) {
  case 2:
    return "0b";
  case -2:
    return "0B";
  case 8:
  case -8:
    return "0";
  case 16:
    return "0x";
  case -16:
    return "0X";
  default:
    return "";
  }
}

/* Check if `number' will produce an infinitely repeating number if
   converted to a string in one of `bases'. If a base is negative, it
   will be treated as if positive. `num_bases' is the number of bases
   in `bases'. If a base is -1, 0, or 1 or `number' is negative,
   `errno' will be set to EINVAL and -1 will be returned. Otherwise,
   the first base in which `number' is found to be terminating will be
   returned. If `number' is not terminating in any base, 0 is
   returned. */
static int is_terminating_in_bases(const mpq_t number,
				   const int *bases, size_t num_bases) {
  mpz_t *den_factors;
  size_t num_den_factors;

  if (num_bases == 0) {
    /* We needn't bother getting the prime factors of the denominator
       since there are no bases to check. */
    return 0;
  }

  /* Get the prime factors of the denominator. */
  if (get_prime_factors_mpz(&den_factors, &num_den_factors,
			    mpq_denref(number))) {
    return -1;
  }

  /* Now check them against all the prime factors of the bases. */
  for (size_t i = 0; i < num_bases; i++) {
    int base = bases[i];
    char sign = 1;
    int *base_factors;
    size_t num_base_factors;
    char terminating = 1;
# ifndef NDEBUG
    int ret;
# endif

    if (base < 0) {
      base = -base;
      sign = -1;
    }

    if (base < 2) {
      /* Invalid base. */
      for (size_t j = 0; j < num_den_factors; j++)
	mpz_clear(den_factors[j]);
      free(den_factors);

      errno = EINVAL;
      return -1;
    }

    /* Get the prime factors of `base'. */
# ifndef NDEBUG
    ret =
# endif
      get_prime_factors(&base_factors, &num_base_factors, base);

    /* This should not happen since we ensured `base' is positive. */
    assert(!ret);

    /* Compare the factors. */
    for (size_t j = 0; j < num_den_factors; j++) {
      terminating = 0;

      for (size_t k = 0; k < num_base_factors; k++) {
	if (mpz_cmp_si(den_factors[j], base_factors[k]) == 0) {
	  /* We've found a match! */
	  terminating = 1;
	  break;
	}
      }

      if (!terminating)
	break;
    }

    free(base_factors);

    if (terminating) {
      /* The number is terminating in this base. We're done! */
      for (size_t j = 0; j < num_den_factors; j++)
	mpz_clear(den_factors[j]);
      free(den_factors);

      return sign*base;
    }
  }

  /* The number does not terminate in any of the bases. Too bad. */
  for(size_t i = 0; i < num_den_factors; i++)
    mpz_clear(den_factors[i]);
  free(den_factors);

  return 0;
}

/* Get the prime factors of an `mpz_t' number. `*factors' will be
   allocated with mu_xmalloc() and the factors will be stored in
   it. The number of factors in `*factors' will be returned in
   `*num_factors'. If `*num_factors' is zero, `*factors' will NOT be
   allocated. If `number' is negative, `errno' will be set to EINVAL,
   and -1 will be returned. Otherwise, zero will be returned.  */
static int get_prime_factors_mpz(mpz_t **factors, size_t *num_factors,
				 const mpz_t number) {
  zlist_t *factor_list, *factor_list_start;
  mpz_t half_number, factor, square, i;
  size_t index;

  if (mpz_cmp_ui(number, 0) < 0) {
    errno = EINVAL;
    return -1;
  }

  mpz_inits(half_number, factor, square, i, NULL);
  mpz_tdiv_q_ui(half_number, number, 2);

  *num_factors = 0;
  factor_list_start = factor_list = NULL;
  for (mpz_set_ui(factor, 2); mpz_cmp(factor, half_number) <= 0;
       mpz_add_ui(factor, factor, 1)) {
    int is_prime = 1;

    if (!mpz_divisible_p(number, factor))
      continue;

    for (mpz_set_ui(i, 2); ; mpz_add_ui(i, i, 1)) {
      mpz_mul(square, i, i);
      if (mpz_cmp(square, factor) > 0)
	break;

      if (mpz_divisible_p(factor, i)) {
	is_prime = 0;
	break;
      }
    }

    if (is_prime) {
      zlist_t *new;

      /* We've found a prime factor! Add it to the list. */
      (*num_factors)++;
      new = mu_xmalloc(sizeof(*new));
      mpz_init_set(new->num, factor);
      new->next = NULL;
      if (factor_list)
	factor_list->next = new;
      else
	factor_list_start = factor_list = new;
    }
  }

  mpz_clears(half_number, factor, square, i, NULL);

  /* Now copy all the factors over from the list. */
  *factors = mu_xmalloc(sizeof(**factors) * *num_factors);
  for (index = 0; factor_list_start; index++) {
    zlist_t *p = factor_list_start;
    assert(index < *num_factors);
    mpz_init_set((*factors)[index], p->num);
    factor_list_start = p->next;
    mpz_clear(p->num);
    free(p);
  }
  assert(index == *num_factors);

  return 0;
}

/* Just like get_prime_factors_mpz(), except for plain old `int'. */
static int get_prime_factors(int **factors, size_t *num_factors, int number) {
  ilist_t *factor_list, *factor_list_start;
  int factor, i;
  size_t index;

  if (number < 0) {
    errno = EINVAL;
    return -1;
  }

  *num_factors = 0;
  factor_list_start = factor_list = NULL;
  for (factor = 2; factor <= number / 2; factor++) {
    int is_prime = 1;

    if (number % factor)
      continue;

    for (i = 2; i * i <= factor; i++) {
      if (!(factor % i)) {
	is_prime = 0;
	break;
      }
    }

    if (is_prime) {
      ilist_t *new;

      /* We've found a prime factor! Add it to the list. */
      (*num_factors)++;
      new = mu_xmalloc(sizeof(*new));
      new->num = factor;
      new->next = NULL;
      if (factor_list)
	factor_list->next = new;
      else
	factor_list_start = factor_list = new;
    }
  }

  /* Now copy all the factors over from the list. */
  *factors = mu_xmalloc(sizeof(**factors) * *num_factors);
  for (index = 0; factor_list_start; index++) {
    ilist_t *p = factor_list_start;
    assert(index < *num_factors);
    (*factors)[index] = p->num;
    factor_list_start = p->next;
    free(p);
  }
  assert(index == *num_factors);

  return 0;
}
#endif /* HAVE_LIBGMP_ALL */

/* Write the separator (i.e. +--+--+, etc.), returning number of bytes
   written. */
static size_t write_human_separator(size_t cell_width, size_t square_width,
				    FILE *file) {
  size_t bytes_written = 0; /* How many bytes we have written */

  if (putc(JOINT, file) == EOF)
    return 0;

  bytes_written++;

  for (size_t i = 0; i < square_width; i++) {
    for (size_t j = 0; j < cell_width; j++) {
      if (putc(HLINE, file) == EOF)
	return 0;

      bytes_written++;
    }

    if (putc(JOINT, file) == EOF)
      return 0;

    bytes_written++;
  }

  if (putc('\n', file) == EOF)
    return 0;

  bytes_written++;

  return bytes_written;
}

/* Write `nums' in the format "(1, 2, 3)" attempting to wrap at `wrap'
   or don't wrap at all if `wrap' is 0. Returns number of bytes
   written. */
static size_t write_human_other_nums(cellval_t *nums, size_t nums_size,
				     size_t wrap, FILE *file) {
  size_t bytes_written = 0;
  size_t line_length = 0;

  if (putc('(', file) == EOF)
    return 0;

  bytes_written++;
  line_length++;

  /* Sort the list of other nums */
  qsort(nums, nums_size, sizeof(*nums), cellval_compare);

  for (size_t i = 0; i < nums_size; i++) {
    char *str;
    int length;

    /* Get the string to write */
#ifdef HAVE_LIBGMP_ALL
    str = get_optimal_str_mpq(cellval_val(nums[i]));
    if (!str)
      return 0;

    length = strlen(str);
#else /* !HAVE_LIBGMP_ALL */
    switch (nums[i].type) {
    case INT:
      length = asprintf(&str, "%lld", nums[i].i);
      break;
    case FLOAT:
      length = asprintf(&str, "%Lg", nums[i].f);
      break;
    default:
      errno = EINVAL;
      return 0;
    }
#endif /* !HAVE_LIBGMP_ALL */

    if (i) {
      if (putc(',', file) == EOF) {
	free(str);
	return 0;
      }

      bytes_written++;
      line_length++;

      if (wrap && line_length + length + 1 > wrap) {
	/* Wrap the line */
	if (fputs("\n ", file) == EOF) {
	  free(str);
	  return 0;
	}

	bytes_written += 2;
	line_length = 1;
      }
      else {
	if (putc(' ', file) == EOF) {
	  free(str);
	  return 0;
	}

	bytes_written++;
	line_length++;
      }
    }

    if (fputs(str, file) == EOF) {
      free(str);
      return 0;
    }

    bytes_written += length;
    line_length += length;

    free(str);
  }

  /* Print the closing parenthesis */
  if (fputs(")\n", file) == EOF)
    return 0;

  bytes_written += 2;

  return bytes_written;
}

/* Compare two `cellval_t's. Returns an integer less than, equal to,
   or greater than zero if `*firstptr' is less than, equal to, or
   greater than `*secondptr'. Designed to be passed to qsort(). */
static int cellval_compare(const void *firstptr, const void *secondptr) {
  cellval_t first = *(cellval_t *)firstptr,
    second = *(cellval_t *)secondptr;

  /* There is no way to indicate an error to qsort(), so we must
     assert that both `first' and `second' are valid. */
  assert(cellval_valid(first) && cellval_valid(second));

  return cellval_cmp(first, second);
}
