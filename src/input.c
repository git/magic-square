/************************************************************************\
 * Magic Square solves magic squares.                                   *
 * Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>           *
 *                                                                      *
 * This file is part of Magic Square.                                   *
 *                                                                      *
 * Magic Square is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * Magic Square is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with Magic Square.  If not, see                                *
 * <https://www.gnu.org/licenses/>.                                     *
\************************************************************************/

/* input.c -- functions for operating on input */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>
#include <assert.h>
#include <mu/safe.h>

#include "parse.h"
#include "input.h"

int getinput(FILE *file, clist_t **got_chars) {
  int c = getc(file);

  if (c != EOF) {
    /* Add the character to `*got_chars' */
    (*got_chars)->c = c;
    (*got_chars)->next = mu_xmalloc(sizeof(*((*got_chars)->next)));
    (*got_chars)->next->prev = *got_chars;
    *got_chars = (*got_chars)->next;
  }

  return c;
}

int ungetinput(int c, FILE *file, clist_t **got_chars) {
  int ret = ungetc(c, file);

  if (ret != c) {
    /* There was an error, ungetc() should always return EOF on error */
    assert(ret == EOF);
    return EOF;
  }

  /* Pop a character from `*got_chars' */
  *got_chars = (*got_chars)->prev;
  free((*got_chars)->next);

  assert((char)c == (*got_chars)->c);

  return ret;
}

/* Peek at the next character of input without getting it from the
   stream */
int peekinput(FILE *file) {
  int c = getc(file);

  if (c == EOF)
    return EOF;

  ungetc(c, file);

  return c;
}
