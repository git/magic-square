/************************************************************************\
 * Magic Square solves magic squares.                                   *
 * Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>           *
 *                                                                      *
 * This file is part of Magic Square.                                   *
 *                                                                      *
 * Magic Square is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * Magic Square is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with Magic Square.  If not, see                                *
 * <https://www.gnu.org/licenses/>.                                     *
\************************************************************************/

/* main.c -- main source file */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <limits.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#ifdef HAVE_LIBCRASH
# include <crash.h>
#endif /* HAVE_LIBCRASH */
#ifdef HAVE_PROGRESS
# include <time.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <math.h>
#endif /* HAVE_PROGRESS */
#ifdef HAVE_PTHREAD
# include <pthread.h> /* For pthread_exit() */
#endif /* HAVE_PTHREAD */
#include <mu/options.h>
#include <mu/format.h>
#include <mu/safe.h>

#include "square.h"
#include "parse.h"
#include "write.h"
#ifdef HAVE_PROGRESS
# include "progress.h"
# include "intprops.h"
#endif

/* Strings describing whether certain features are built into the
   program. */
#ifdef HAVE_PTHREAD
# define HAVE_PTHREAD_STRING "yes"
#else
# define HAVE_PTHREAD_STRING "no"
#endif
#ifdef HAVE_PROGRESS
# define HAVE_PROGRESS_STRING "yes"
#else
# define HAVE_PROGRESS_STRING "no"
#endif
#ifdef HAVE_LIBCRASH
# define HAVE_LIBCRASH_STRING "yes"
#else
# define HAVE_LIBCRASH_STRING "no"
#endif
#if defined HAVE_LIBGMP_ALL
# define HAVE_LIBGMP_STRING "yes (all)"
#elif defined HAVE_LIBGMP_PROGRESS
# define HAVE_LIBGMP_STRING "yes (progress only)"
#elif defined HAVE_LIBGMP
# error HAVE_LIBGMP is defined but neither 		\
  HAVE_LIBGMP_PROGRESS or HAVE_LIBGMP_ALL is defined
# define HAVE_LIBGMP_STRING "ERROR!!!"
#else
# define HAVE_LIBGMP_STRING "no"
#endif

/* A string putting all the above together. */
#define PACKAGE_FEATURES						\
  "Features:\n"								\
  "\tMulti-threading:\t\t\t" HAVE_PTHREAD_STRING "\n"			\
  "\tProgress indication:\t\t\t" HAVE_PROGRESS_STRING "\n"		\
  "\tCrash reporting:\t\t\t" HAVE_LIBCRASH_STRING "\n"			\
  "\tArbitrary precision arithmetic (GMP):\t" HAVE_LIBGMP_STRING "\n"

#define STR(x) #x
#define XSTR(x) STR(x)

#define NOTES "Report bugs to <" PACKAGE_BUGREPORT ">."

#ifdef HAVE_PROGRESS
/* The default progress interval in milliseconds. */
# define PROGRESS_INTERVAL_MS 250
/* The default progress interval in nanoseconds. */
# define PROGRESS_INTERVAL_NS (PROGRESS_INTERVAL_MS * 1000000)
/* The default progress interval as a `struct timespec'. This will
   need to be changed if PROGRESS_INTERVAL_MS >= 1000. */
# define PROGRESS_INTERVAL_TS { .tv_sec = 0, .tv_nsec = PROGRESS_INTERVAL_NS }
#endif

/* Flags for `keep_going'. */
#define KEEP_GOING	0b00000001
#define DO_TRIVIAL	0b00000010

enum filespec { NONE, INFILE, OUTFILE };

/* Data structures to pass to callbacks. */

struct square_format_data {
  enum file_format *in;
  enum file_format out;
  const char **infilenames;
  int num_files;
  enum filespec last_file;
};

struct squarec_format_data {
  enum file_format in;
  enum file_format out;
  const char *infilename;
  enum filespec last_file;
};

struct jobs_data {
  int max_threads_set;
  unsigned long max_threads;
};

#ifdef HAVE_PROGRESS
struct progress_data {
  int enabled;
  progress_mode_t mode;
  struct timespec interval;
};
#endif

/* Static helper functions */
static int square_main(int, char **);
static int squarec_main(int, char **);
static int do_keep_going(int, void *, char *);
static int do_trivial(int, void *, char *);
static int square_do_format(int, int, void *, char *);
static int square_set_last_file_output(int, const char *,
				       void *, char *);
static int squarec_do_format(int, int, void *, char *);
static int squarec_set_last_file_output(int, const char *,
					void *, char *);
static int parse_jobs(int, const char *, void *, char *);
static int parse_progress(int, const char *, void *, char *);
#ifdef HAVE_PROGRESS
static int do_quiet(void *, char *);
#endif
static int print_version(void *, char *);
static int square_set_input_file(const char *, void *, char *);
static int squarec_set_input_file(const char *, void *, char *);
static enum file_format get_format(FILE *, const char *);
#ifdef HAVE_PROGRESS
static int same_file(FILE *, FILE *, const char *, const char *);
static int fisatty(FILE *, const char *);
#endif

static const MU_ENUM_VALUE format_values[] = {
  { "text",	TEXT   },
  { "binary",	BINARY },
  { 0 }
};

static const MU_OPT version_opt = {
  .short_opt	= "v",
  .long_opt	= "version",
  .has_arg	= MU_OPT_NONE,
  .callback_none = print_version,
  .help		= "print version information and exit"
};

/* Call the correct main() function based on the name we were run as */
int main(int argc, char **argv) {
  char *progname, *bname;
  enum { SQUARE, SQUAREC } mode;
#ifdef HAVE_LIBCRASH
  int ret;

  /* Initialize crash reporting */
  ret = crash_init(argv[0], PACKAGE_STRING, PACKAGE_BUGREPORT, NULL);

  if (ret)
    return ret;
#endif /* HAVE_LIBCRASH */

  /* Get the mode */
  progname = mu_xstrdup(argv[0]); /* Make a copy since basename(3) can
				     modify its argument */
  bname = basename(progname);
  mode = strcasecmp(bname, "squarec") ? SQUARE : SQUAREC;

  free(progname);

  return (mode == SQUARE) ?
    square_main(argc, argv) :
    (mode == SQUAREC) ?
    squarec_main(argc, argv) :
    -1;
}

/***************************\
|* Static helper functions *|
\***************************/

static int square_main(int argc, char **argv) {
  int keep_going = 0;
  int num_error = 0; /* How many squares we we're unable to solve */
  /* This will need to hold at most `argc - 1' strings */
  const char *infilenames[argc - 1];
  /* The format of each file in `infilenames'. Note that if `argc' is
     1, we still need to store the format of stdin. */
  enum file_format informats[argc - (argc > 1)];
  FILE *outfile;
  const char *outfilename;
  struct square_format_data format_data = {
    .in			= informats,
    .out		= UNKNOWN,
    .infilenames	= infilenames,
    .num_files		= 0,
    .last_file		= NONE
  };
#ifdef HAVE_PTHREAD
  struct jobs_data jobs = { 0 };
#endif
#ifdef HAVE_PROGRESS
  FILE *progress_file = NULL;
  char *progress_filename;
  struct progress_data progress = {
    .enabled	= 2,
    .mode	= AUTO,
    .interval	= PROGRESS_INTERVAL_TS
  };
#endif
  const MU_OPT options[] = {
    {
     .short_opt	= "k",
     .long_opt	= "keep-going",
     .has_arg	= MU_OPT_NONE,
     .negatable	= 1,
     .callback_negatable = do_keep_going,
     .cb_data	= &keep_going,
     .help	= "keep going after the first solution"
    },
    {
     .short_opt	= "a",
     .long_opt	= "all",
     .has_arg	= MU_OPT_NONE,
     .negatable	= 1,
     .callback_negatable = do_trivial,
     .cb_data	= &keep_going,
     .help	=
     "print trivial differences (rotations/reflections); "
     "implies --keep-going"
    },
    {
     .short_opt	= "f",
     .long_opt	= "format",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_ENUM,
     .enum_values = format_values,
     .callback_enum = square_do_format,
     .cb_data	= &format_data,
     .help	=
     "parse FILE as text or binary; this applies to FILE if "
     "given after FILE, to OUTFILE if given after OUTFILE (see -o) "
     "or to standard input in the case that no files are given"
    },
    {
     .short_opt	= "o",
     .long_opt	= "output",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .argstr	= &outfilename,
     .callback_string = square_set_last_file_output,
     .cb_data	= &format_data,
     .arg_help	= "OUTFILE",
     .help	=
     "write to OUTFILE instead of standard "
     "output (may be followed by --format)"
    },
    {
     .short_opt	= "j",
     .long_opt	= "jobs|threads",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = parse_jobs,
#ifdef HAVE_PTHREAD
     .cb_data	= &jobs,
     .arg_help	= "THREADS",
     .help	=
     "use at most THREADS threads when solving "
     "(0 is the same as 1); if not given or 'auto', "
     "automatically regulate the number of threads"
#endif
    },
    {
     .short_opt	= "p",
     .long_opt	= "progress",
     .has_arg	= MU_OPT_OPTIONAL,
     .arg_type	= MU_OPT_STRING,
     .callback_string = parse_progress,
#ifdef HAVE_PROGRESS
     .cb_data	= &progress,
     .arg_help	= "MODE[:INTERVAL]",
     .help	=
     "display a progress bar; if MODE is 'bar' (the "
     "default if output is a tty), print a progress bar, "
     "updating every INTERVAL milliseconds (default "
     XSTR(PROGRESS_INTERVAL_MS) "); if MODE is 'dot' (the "
     "default if output is not a tty), print a '.' for "
     "each 0.1% done (updating every INTERVAL milliseconds)"
#endif
    },
    {
     .short_opt	= "q",
     .long_opt	= "quiet|no-progress",
     .has_arg	= MU_OPT_NONE,
#ifdef HAVE_PROGRESS
     .callback_none = do_quiet,
     .cb_data	= &progress,
     .help	= "don't display a progress indicator"
#else
     /* No need for a callback; we can just silently ignore this since
	this is how we would behave anyway. */
#endif
    },
    {
     .long_opt	= "progress-file",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
#ifdef HAVE_PROGRESS
     .arg	= &progress_filename,
     .arg_help	= "FILE|auto",
     .help	=
     "write the progress bar to FILE, or automatically determine "
     "an appropriate file if 'auto' or not given (use './auto' to "
     "refer to a file named literally 'auto'); implies --progress"
#else
     /* Print "progress indication is disabled". */
     .callback_string = parse_progress
#endif
    },
    { 0 }, { 0 },		/* The help and man options */
    version_opt,
    { 0 }
  };
  MU_OPT_CONTEXT *context;
  const char *const desc = "Solve each FILE as a magic square.\n\n"
    "With no FILE, or when FILE is -, read standard input. "
    "Default format (see -f) is 'text' if extension is not '.bin' "
    "or FILE (stdin if not given) is a tty, 'binary' otherwise.";
  int ret;

  context = mu_opt_context_xnew(argc, argv, options, MU_OPT_BUNDLE);
  mu_opt_context_add_help(context,
			  "[OPTION]... [FILE [-f FORMAT]]...",
			  "solve magic squares", desc, NOTES,
			  "6", NULL, PACKAGE_STRING, "2020-06-18");
  mu_opt_context_xadd_help_options(context,
				   MU_HELP_BOTH | MU_HELP_MAN_BOTH);
  mu_opt_context_set_arg_callback(context, square_set_input_file,
				  &format_data, NULL);

  /* Initialize `format_data.in[0]' because we will need to test
     whether it was set before any files were specified. */
  format_data.in[0] = UNKNOWN;

  /* Parse the options. */
  ret = mu_parse_opts(context);
  if (MU_OPT_ERR(ret)) {
    if (ret == MU_OPT_ERR_IO)
      return 2;
    mu_format_help(stderr, context);
    return 1;
  }
  mu_opt_context_xfree(context);

  /* Open the output file */
  if (!outfilename || !strcmp(outfilename, "-")) {
    outfile = stdout;
    outfilename = "<stdout>";
  }
  else {
    outfile =
      fopen(outfilename, (format_data.out == BINARY) ? "wb" : "w");
    if (!outfile) {
      fprintf(stderr, "%s: cannot open %s for writing: %m\n",
	      argv[0], outfilename);

      return 2;
    }
  }

  /* Get the output format if it wasn't specified explicitly. */
  if (format_data.out == UNKNOWN) {
    format_data.out = get_format(outfile, outfilename);
    if (format_data.out == UNKNOWN) {
      fclose(outfile);
      return 2;
    }
  }

#ifdef HAVE_PROGRESS
  if (progress.enabled > 1 && !keep_going) {
    /* The progress indicator won't be very meaningful if we're only
       finding one solution, so disable it. */
    progress.enabled = 0;
  }
  else if (progress.enabled && !progress_file) {
    if (progress_filename) {
      /* Open the progress file. */
      if (!strcmp(progress_filename, "-")) {
	progress_file = stdout;
	progress_filename = "<stdout>";
      }
      else {
	if (!(progress_file = fopen(progress_filename, "w"))) {
	  fprintf(stderr, "%s: cannot open %s for writing: %m\n",
		  argv[0], progress_filename);

	  return 2;
	}
      }
    }
    else {
      /* Attempt to determine a good file to write the progress
	 bar. */
      if (same_file(outfile, stdout, outfilename, "<stdout>")) {
	if (same_file(stdout, stderr, "<stdout>", "<stderr>")) {
	  if (progress.enabled > 1)
	    progress.enabled = 0;
	  else {
	    progress_file = stdout;
	    progress_filename = "<stdout>";
	  }
	}
	else {
	  if (progress.enabled > 1 && !fisatty(stderr, "<stderr>")) {
	    progress.enabled = 0;
	  }
	  else {
	    progress_file = stderr;
	    progress_filename = "<stderr>";
	  }
	}
      }
      else {
	if (fisatty(stdout, "<stdout>")) {
	  progress_file = stdout;
	  progress_filename = "<stdout>";
	}
	else if (fisatty(stderr, "<stderr>")) {
	  if (progress.enabled > 1 &&
	      same_file(outfile, stderr, outfilename, "<stderr>")) {
	    progress.enabled = 0;
	  }
	  else {
	    progress_file = stderr;
	    progress_filename = "<stderr>";
	  }
	}
	else {
	  /* Default to stdout. */
	  progress_file = stdout;
	  progress_filename = "<stdout>";
	}
      }
    }
  }

  /* If the progress mode was not set explicitly, use BAR if the file
     is a tty, DOT otherwise. */
  if (progress.enabled && progress.mode == AUTO)
    progress.mode =
      fisatty(progress_file, progress_filename) ? BAR : DOT;
#endif /* !HAVE_PROGRESS */

#ifdef HAVE_PTHREAD
  if (!jobs.max_threads_set) {
    /* Determine optimum number of threads for this machine. */
    long opt_threads = sysconf(_SC_NPROCESSORS_ONLN);

    if (opt_threads < 0) {
      fprintf(stderr,
	      "%s: unable to determine optimum number of threads: %m "
	      "(hint: try -j THREADS)\n", argv[0]);

      return -1;
    }

    if (!opt_threads) {
      fprintf(stderr, "%s: you appear to have no available CPU(s)!\n",
	      argv[0]);

      return -1;
    }

    /* Remember, this is the number of *new* threads to start. We are
       a thread. */
    jobs.max_threads = opt_threads - 1;
  }
#endif

  /* If we have no files, iterate once but with `i' set to -1 to
     indicate that fact. */
  for (int i = -(format_data.num_files < 1);
       i < format_data.num_files; i++) {
    square_t square;
    FILE *infile;
    const char *infilename;
    enum file_format format = format_data.in[(i == -1) ? 0 : i];
    int ret;

    if (i == -1 || !strcmp(infilenames[i], "-")) {
      infile = stdin;
      infilename = "<stdin>";
    }
    else {
      infilename = infilenames[i];

      if (!(infile = fopen(infilename, (format == BINARY) ? "rb" : "r"))) {
	fprintf(stderr, "%s: cannot open %s for reading: %m\n",
		argv[0], infilename);
	num_error++;

	continue;
      }
    }

    if (format == UNKNOWN) {
      format = get_format(infile, infilename);
      if (format == UNKNOWN) {
	fclose(infile);
	num_error++;
	continue;
      }
    }

    if ((format == TEXT) ?
	parse_human(&square, infile) :
	parse_machine(&square, infile)) {
#ifndef HAVE_LIBGMP_ALL
      if (format == BINARY && errno == ENOTSUP)
	fprintf(stderr,
		"%s: %s contains GMP types, "
		"but GMP was disabled in this version\n",
		argv[0], infilename);
      else
#endif
	fprintf(stderr, "%s: cannot parse %s: %m\n", argv[0], infilename);

      num_error++;

      continue;
    }

    if (infile != stdin && fclose(infile)) {
      fprintf(stderr, "%s: cannot close %s: %m\n", argv[0], infilename);
      num_error++;

      continue;
    }

    if (format_data.num_files > 1 && format_data.out == TEXT) {
      /* Indicate which file we are solving */

      size_t infilename_len = strlen(infilename);

      if (i > num_error) {
	/* Separate from the last file */
	putc('\n', outfile);
      }

      fputs(infilename, outfile);
      putc('\n', outfile);
      for (size_t i = 0; i < infilename_len; i++)
	putc('=', outfile);
      fputs("\n\n", outfile);
    }

    /* Solve the square */
    ret = square_solve(&square, NULL, NULL,
		       keep_going & KEEP_GOING,
		       !(keep_going & DO_TRIVIAL),
		       outfile, format_data.out
#ifdef HAVE_PTHREAD
		       , jobs.max_threads, !jobs.max_threads_set
#endif
#ifdef HAVE_PROGRESS
		       , progress.enabled, &progress.interval,
		       progress_file, progress_filename, progress.mode
#endif
		       );

    /* We're done with this */
    square_destroy(&square);

    if (ret) {
      fprintf(stderr, "%s: unable to solve square: %m\n", argv[0]);
      num_error++;
    }
  }

#ifdef HAVE_PTHREAD
  /* We cannot terminate with a nonzero exit status using
     pthread_exit(). If we need to do that, we must return instead. */
  if (num_error)
    return num_error;

  pthread_exit(NULL);
#else /* !HAVE_PTHREAD */
  return num_error;
#endif /* !HAVE_PTHREAD */
}

static int squarec_main(int argc, char **argv) {
  square_t square;
  FILE *infile, *outfile;
  const char *infilename, *outfilename = NULL;
  struct squarec_format_data format_data = {
    .in		= UNKNOWN,
    .out	= UNKNOWN,
    .infilename	= NULL,
    .last_file	= NONE
  };
  MU_OPT options[] = {
    {
     .short_opt	= "f",
     .long_opt	= "format",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_ENUM,
     .enum_values = format_values,
     .callback_enum = squarec_do_format,
     .cb_data	= &format_data,
     .help	=
     "parse a file as text or binary; this "
     "applies to INFILE if given after INFILE, or "
     "to OUTFILE if given after OUTFILE (see -o)"
    },
    {
     .short_opt	= "o",
     .long_opt	= "output",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .argstr	= &outfilename,
     .callback_string = squarec_set_last_file_output,
     .cb_data	= &format_data,
     .arg_help	= "OUTFILE",
     .help	=
     "write to OUTFILE instead of standard "
     "output (may be followed by --format)"
    },
    { 0 }, { 0 },		/* The help and man options */
    version_opt,
    { 0 }
  };
  MU_OPT_CONTEXT *context;
  int ret;

  context = mu_opt_context_xnew(argc, argv, options, MU_OPT_BUNDLE);
  mu_opt_context_add_help(context, "[OPTION]... "
			  "[-o OUTFILE [-f FORMAT]] "
			  "[INFILE [-f FORMAT]]",
			  "convert magic square formats",
			  "Read INFILE and output to OUTFILE, "
			  "possibly converting between formats.",
			  NOTES, "6", NULL, PACKAGE_STRING,
			  "2020-06-18");
  mu_opt_context_xadd_help_options(context,
				   MU_HELP_BOTH | MU_HELP_MAN_BOTH);
  mu_opt_context_set_arg_callback(context, squarec_set_input_file,
				  &format_data, NULL);

  /* Parse the options. */
  ret = mu_parse_opts(context);
  if (MU_OPT_ERR(ret)) {
    if (ret == MU_OPT_ERR_IO)
      return 2;
    mu_format_help(stderr, context);
    return 1;
  }
  mu_opt_context_xfree(context);

  /* Get the input file */
  infilename = format_data.infilename;
  if (!infilename || !strcmp(infilename, "-")) {
    infilename = "<stdin>";
    infile = stdin;
  }
  else {
    /* Open the input file (if the input format was not specified
       explicitly, we can't know what mode to use ("r" or "rb") until
       we determine the input format, but it's good if it's buggy on
       Windoze). */
    infile =
      fopen(infilename, (format_data.in == BINARY) ? "rb" : "r");
    if (!infile) {
      fprintf(stderr, "%s: cannot open %s for reading: %m\n",
	      argv[0], infilename);

      return 2;
    }
  }

  /* Get the output file */
  if (!outfilename || !strcmp(outfilename, "-")) {
    outfilename = "<stdout>";
    outfile = stdout;
  }
  else {
    /* Open the output file (same as above (we can't know what mode to
       use), except this time we should default to assuming it will be
       binary since the output file will probably be binary while the
       input file will probably be text). */
    outfile =
      fopen(outfilename, (format_data.out == TEXT) ? "w" : "wb");
    if (!outfile) {
      fprintf(stderr, "%s: cannot open %s for writing: %m\n",
	      argv[0], outfilename);

      return 2;
    }
  }

  /* Get the formats if they weren't specified explicitly */
  if (format_data.in == UNKNOWN) {
    format_data.in = get_format(infile, infilename);
    if (format_data.in == UNKNOWN) {
      fclose(infile);
      return 2;
    }
  }

  if (format_data.out == UNKNOWN) {
    format_data.out = get_format(outfile, outfilename);
    if (format_data.out == UNKNOWN) {
      fclose(outfile);
      return 2;
    }
  }

  /* Now parse the square */
  if (((format_data.in == TEXT) ?
       parse_human :
       parse_machine)(&square, infile)) {
#ifndef HAVE_LIBGMP_ALL
      if (format_data.in == BINARY && errno == ENOTSUP)
	fprintf(stderr,
		"%s: %s contains GMP types, "
		"but GMP was disabled in this version\n",
		argv[0], infilename);
      else
#endif
	fprintf(stderr, "%s: cannot parse %s: %m\n", argv[0], infilename);

    return 3;
  }

  if (fclose(infile)) {
    fprintf(stderr, "%s: cannot close %s: %m\n", argv[0], infilename);
    return 2;
  }

  /* And write the output file */
  if (!((format_data.out == TEXT) ?
	write_human :
	write_machine)(&square, outfile)) {
    fprintf(stderr, "%s: cannot write %s: %m\n", argv[0], outfilename);
    return 3;
  }

  if (fclose(outfile)) {
    fprintf(stderr, "%s: cannot write %s: %m\n", argv[0], outfilename);
    return 3;
  }

  square_destroy(&square);
  return 0;
}

/* Don't stop after the first solution. */
static int do_keep_going(int value, void *data, char *err) {
  int *keep_going = data;
  if (value)
    *keep_going |= KEEP_GOING;
  else
    *keep_going &= ~KEEP_GOING;
  return 0;
}

/* Treat squares as different even if the differences are trivial. */
static int do_trivial(int value, void *data, char *err) {
  int *keep_going = data;
  if (value)
    *keep_going |= KEEP_GOING | DO_TRIVIAL;
  else
    *keep_going &= ~DO_TRIVIAL;
  return 0;
}

/* Set the input or output format for the last file specified. */
static int square_do_format(int has_arg, int arg,
			    void *data, char *err) {
  struct square_format_data *format = data;

  switch (format->last_file) {
  case NONE:
    /* Assume no files are specified so we are parsing stdin. */
    format->in[0] = arg;
    break;

  case INFILE:
    assert(format->num_files > 0);
    /* Set the input format for the current file */
    format->in[format->num_files - 1] = arg;
    break;

  case OUTFILE:
    /* Set the format for the output file */
    format->out = arg;
    break;

  default:
    assert(0);
  }

  return 0;
}

/* Set `((struct square_format_data *)data)->last_file' to OUTFILE. */
static int square_set_last_file_output(int has_arg, const char *arg,
				       void *data, char *err) {
  struct square_format_data *format = data;
  format->last_file = OUTFILE;
  format->out = UNKNOWN;
  return 0;
}

/* Set the input or output format for the last file specified. */
static int squarec_do_format(int has_arg, int arg,
			     void *data, char *err) {
  struct squarec_format_data *format = data;

  switch (format->last_file) {
  case NONE:
    /* Assume no files are specified so we are parsing stdin. */
  case INFILE:
    /* Set the input format for the input file */
    format->in = arg;
    break;

  case OUTFILE:
    /* Set the format for the output file */
    format->out = arg;
    break;

  default:
    assert(0);
  }

  return 0;
}

/* Set `((struct squarec_format_data *)data)->last_file' to
   OUTFILE. */
static int squarec_set_last_file_output(int has_arg, const char *arg,
					void *data, char *err) {
  struct squarec_format_data *format = data;
  format->last_file = OUTFILE;
  format->out = UNKNOWN;
  return 0;
}

/* Parse a job specification (number of jobs or 'auto'). */
static int parse_jobs(int has_arg, const char *arg,
		      void *data, char *err) {
#ifdef HAVE_PTHREAD
  struct jobs_data *jobs = data;
  char *endptr;

  if (!strncasecmp(arg, "auto", strlen(arg))) {
    jobs->max_threads_set = 0; /* In case it was set earlier */
    return 0;
  }

  errno = 0;
  jobs->max_threads = strtoul(arg, &endptr, 0);

  if (errno) {
    snprintf(err, MU_OPT_ERR_MAX, "too many threads (%s)", arg);
    return 1;
  }

  if (*endptr) {
    snprintf(err, MU_OPT_ERR_MAX, "%s: not an integer", arg);
    return 1;
  }

  /* Don't forget that we are a thread too! */
  if (jobs->max_threads)
    jobs->max_threads--;

  jobs->max_threads_set = 1;

  return 0;
#else /* !HAVE_PTHREAD */
  strncpy(err, "no thread support", MU_OPT_ERR_MAX);
  return 1;
#endif /* !HAVE_PTHREAD */
}

/* Enable progress and get the mode/interval. */
static int parse_progress(int has_arg, const char *arg,
			  void *data, char *err) {
#ifdef HAVE_PROGRESS
  struct progress_data *progress = data;

  progress->enabled = 1;

  if (has_arg) {
    char *interval_ptr;
    double progress_interval_ms;

    /* Find the interval part of the string and separate it into a
       new string if found. */
    if ((interval_ptr = strchr(arg, ':')))
      *(interval_ptr++) = '\0';

    /* Parse `optarg' to get the mode. */
    progress->mode = progress_parse_mode(optarg);
    if (progress->mode == ERROR) {
      snprintf(err, MU_OPT_ERR_MAX, "invalid progress mode: %s", arg);
      return 1;
    }

    if (interval_ptr) {
      char *endptr;

      /* Parse `inteval_ptr' to get the interval. */
      errno = 0;
      progress_interval_ms = strtod(interval_ptr, &endptr);

      if (progress_interval_ms < 0) {
	snprintf(err, MU_OPT_ERR_MAX,
		 "interval cannot be negative: %s", arg);
	return 1;
      }

      if (errno) {
	snprintf(err, MU_OPT_ERR_MAX, "interval too large: %s", arg);
	return 1;
      }

      switch (fpclassify(progress_interval_ms)) {
      case FP_NAN:
	snprintf(err, MU_OPT_ERR_MAX,
		 "interval cannot be NaN: %s", optarg);
	return 1;
      case FP_INFINITE:
	snprintf(err, MU_OPT_ERR_MAX,
		 "interval must be finite: %s", arg);
	return 1;
      }

      if (*endptr) {
	snprintf(err, MU_OPT_ERR_MAX, "%s: not an integer", arg);
	return 1;
      }

      if (progress_interval_ms / 1000 > TYPE_MAXIMUM(time_t)) {
	snprintf(err, MU_OPT_ERR_MAX, "interval too large: %s", arg);
	return 1;
      }

      /* Convert the interval to a `struct timespec'. */
      for (progress->interval.tv_sec = 0;
	   progress_interval_ms >= 1000;
	   progress_interval_ms -= 1000) {
	progress->interval.tv_sec++;
      }
      progress->interval.tv_nsec = progress_interval_ms = 1000000;
    }
  }

  return 0;
#else /* !HAVE_PROGRESS */
  strncpy(err, "progress indication is disabled", MU_OPT_ERR_MAX);
  return 1;
#endif /* !HAVE_PROGRESS */
}

#ifdef HAVE_PROGRESS
/* Disable progress. */
static int do_quiet(void *data, char *err) {
  struct progress_data *progress = data;
  progress->enabled = 0;
  return 0;
}
#endif

/* Print version information and exit. */
static int print_version(void *data, char *err) {
  unsigned short col = 0;

  if (mu_format(stdout, &col, 70, 80, 0, 0,
PACKAGE_STRING "\n\n"
PACKAGE_FEATURES "\n\
Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>\n\n"
PACKAGE_NAME " comes with ABSOLUTELY NO WARRANTY. You may \
redistribute copies of " PACKAGE_NAME " under the terms of the \
GNU General Public License version 3, or (at your option) any later \
version. For more information about these matters, see the file \
named COPYING.\n\n"
NOTES "\n")) {
    snprintf(err, MU_OPT_ERR_MAX, "cannot print version message: %m");
    return 1;
  }

  exit(0);
}

/* Add another input file for `square'. */
static int square_set_input_file(const char *file,
				 void *data, char *err) {
  struct square_format_data *d = data;

  /* Check if `-f' was given before the file. */
  if (!d->num_files && d->in[0] != UNKNOWN) {
    strncpy(err, "-f must not be given before any files",
	    MU_OPT_ERR_MAX);
    return 1;
  }

  d->infilenames[d->num_files] = file;
  d->in[d->num_files] = UNKNOWN;
  d->num_files++;

  d->last_file = INFILE;

  return 0;
}

/* Set the input file for `squarec'. */
static int squarec_set_input_file(const char *file,
				  void *data, char *err) {
  struct squarec_format_data *d = data;

  /* Check if `-f' was given before the file. */
  if (d->in != UNKNOWN) {
    strncpy(err, "-f must not be given before any files",
	    MU_OPT_ERR_MAX);
    return 1;
  }

  if (d->infilename) {
    strncpy(err, "only one input file may be specified",
	    MU_OPT_ERR_MAX);
    return 1;
  }

  d->infilename = file;
  d->last_file = INFILE;
  assert(d->in == UNKNOWN);

  return 0;
}

/* Get the format based on the extension of the file and whether it is
   a tty. On error, returns UNKNOWN and prints an error message. If
   `file == stdin' (or stdout if it's the output file). */
static enum file_format get_format(FILE *file, const char *name) {
  int fd = fileno(file);
  int flags = fcntl(fd, F_GETFL);
  char is_stdio =
    (flags & O_WRONLY) ? (fd == STDOUT_FILENO) :
    (flags & O_RDWR) ? (fd == STDIN_FILENO || fd == STDOUT_FILENO) :
    /* O_RDONLY */ (fd == STDIN_FILENO);
  char *extension = (is_stdio) ? NULL : strrchr(name, '.');

  /* This shouldn't happen because `file' should be valid. */
  assert(fd >= 0);

  /* Get the format based on whether the file is a tty. */
  if (isatty(fd)) {
    /* Yes, I know. I should re open the file as mode "r" instead of
       "rb". But screw those M$ Windoze lusers! (You're not one, are
       you?) */

    return TEXT;
  }

  if (errno != ENOTTY) {
    fprintf(stderr, "%s: unable to determine if %s is a tty: %m\n",
	    program_invocation_name, name);

    return UNKNOWN;
  }

  /* It's not a tty, so get the format based on its extension if it's
     not stdin or stdout (depending on the flags). */
  if (is_stdio || (extension && !strcmp(extension, ".bin")))
    return BINARY;

  /* If the extension is not `.bin' or there is no extension and the
     file is not stdin (in which case it should be BINARY if it's not
     a tty), default to TEXT. */
    return TEXT;
}

#ifdef HAVE_PROGRESS
/* Determine if `a' is the same file as `b'. */
static int same_file(FILE *a, FILE *b,
		     const char *name_a, const char *name_b) {
  int fd_a, fd_b;
  struct stat stat_a, stat_b;

  /* Get the file descriptors. */
  if ((fd_a = fileno(a)) == -1) {
    /* This shouldn't happen because `a' should be valid. */
    fprintf(stderr, "%s: unable to get file descriptor of %s: %m\n",
	    program_invocation_name, name_a);
    exit(-1);
  }
  if ((fd_b = fileno(b)) == -1) {
    /* This shouldn't happen because `b' should be valid. */
    fprintf(stderr, "%s: unable to get file descriptor of %s: %m\n",
	    program_invocation_name, name_b);
    exit(-1);
  }

  /* Stat the files. */
  if (fstat(fd_a, &stat_a) == -1) {
    fprintf(stderr, "%s: cannot stat %s: %m\n",
	    program_invocation_name, name_a);
    exit(2);
  }
  if (fstat(fd_b, &stat_b) == -1) {
    fprintf(stderr, "%s: cannot stat %s: %m\n",
	    program_invocation_name, name_b);
    exit(2);
  }

  /* Check if they're the same file. */
  return stat_a.st_ino == stat_b.st_ino;
}

/* Determine if a FILE * is a tty. */
static int fisatty(FILE *file, const char *name) {
  int fd = fileno(file);

  if (fd == -1) {
    /* This shouldn't happen because `file' should be valid. */
    fprintf(stderr, "%s: cannot get file descriptor for %s: %m\n",
	    program_invocation_name, name);
    exit(-1);
  }

  if (isatty(fd))
    return 1;

  if (errno != ENOTTY) {
    fprintf(stderr, "%s: unable to determine if %s is a tty: %m\n",
	    program_invocation_name, name);

    exit(2);
  }

  return 0;
}
#endif /* HAVE_PROGRESS */
