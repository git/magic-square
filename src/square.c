/************************************************************************\
 * Magic Square solves magic squares.                                   *
 * Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>           *
 *                                                                      *
 * This file is part of Magic Square.                                   *
 *                                                                      *
 * Magic Square is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * Magic Square is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with Magic Square.  If not, see                                *
 * <https://www.gnu.org/licenses/>.                                     *
\************************************************************************/

/* square.c -- functions for operating on magic squares */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <assert.h>
#if defined HAVE_PROGRESS || defined HAVE_PTHREAD
# include <time.h>
#endif /* HAVE_PROGRESS || HAVE_PTHREAD */
#ifdef HAVE_PTHREAD
# include <pthread.h>
# ifdef HAVE_LIBCRASH
#  include <crash.h>
# endif /* HAVE_LIBCRASH */
#endif /* HAVE_PTHREAD */
#ifdef HAVE_LIBGMP_ALL
# include <gmp.h>
#endif
#include <mu/safe.h>

#include "square.h"
#include "write.h"

#ifdef HAVE_PROGRESS
/* The clock to use when checking if we should display the progress
   bar (or sleeping if we have pthreads). */
# define PROGRESS_CLOCK CLOCK_MONOTONIC
#endif /* HAVE_PROGRESS */

#ifdef HAVE_PTHREAD
/* Sometimes we have to capture the return value of multiple functions
   only if !NDEBUG. So then we need to separate them with `||' rather
   than `;'. */
# ifdef NDEBUG
#  define STMT_SEPARATOR ;
# else /* !NDEBUG */
#  define STMT_SEPARATOR ||
# endif

/* Used when getting the CPU usage. */
# define CPU_USAGE_MAX 1000

/* A structure containing all the arguments to pass to
   square_solve_recursive(). */
struct solve_args {
  square_t *square;
  cellval_t sum;
  square_list_t **solutions;
  size_t *solutions_size;
  char keep_going;
  char skip_trivial;
  FILE *file;
};

/* A structure to pass to tryunlock_mutex(). */
struct tryunlock_arg {
  pthread_mutex_t *mutex;
  char do_unlock;
};

/* A thread-independent errno */
static int global_errno;
/* Mutexes for square_solve_recursive() */
static pthread_mutex_t solutions_mutex,
  threads_left_mutex, global_errno_mutex
# ifdef HAVE_PROGRESS
  , progress_mutex, progress_file_mutex
# endif
  ;
/* Condition variables to tell the main thread when a thread starts or
   exits. */
static pthread_cond_t threads_left_changed, thread_exit;
/* Number of threads left available */
static unsigned long threads_left;
static FILE *proc_stat;
# ifdef HAVE_PROGRESS
/* Whether the progress file is the same as the output file. */
static char progress_is_output;
# endif /* HAVE_PROGRESS */
#endif /* HAVE_PTHREAD */
/* The format with which to write the square */
static enum file_format format;
/* The function with which to write a square */
static size_t (*write_func)(const square_t *, FILE *);
#if defined HAVE_PTHREAD
# ifdef HAVE_LIBCRASH
/* The auto-regulation thread. */
static pthread_t regulation_thread;
# endif /* HAVE_LIBCRASH */
/* Whether we are regulating threads. */
static char auto_regulate_threads;
#endif /* HAVE_PTHREAD */
#ifdef HAVE_PROGRESS
/* If and when to report progress. */
static char progress;
static const struct timespec *progress_interval;
/* The current iteration we are on. */
static progress_t progress_iteration;
# ifdef HAVE_PTHREAD
#  ifdef HAVE_LIBCRASH
/* The progress display thread. */
static pthread_t progress_thread;
#  endif /* HAVE_LIBCRASH */
# else /* !HAVE_PTHREAD */
/* The next time to print the progress. */
static struct timespec next_progress_time;
# endif /* !HAVE_PTHREAD */
#endif /* HAVE_PROGRESS */

/* Static helper functions */
static int square_solve_recursive(square_t *, cellval_t, square_list_t **,
				  size_t *, char, char, FILE *);
#ifdef HAVE_PTHREAD
static int square_solve_new_thread(square_t *, cellval_t, square_list_t **,
				   size_t *, char, char, FILE *);
static void * square_solve_unpack_args(void *);
static void tryunlock_mutex(void *);
static void * regulate_threads_wrapper(void *);
static void close_proc_stat(void *);
static void regulate_threads(unsigned long *);
static int get_cpu_usage(const struct timespec *);
static int get_cpu_times(long *, long *);
# ifdef HAVE_PROGRESS
static void * display_progress_thread(void *);
# endif /* HAVE_PROGRESS */
# ifdef HAVE_LIBCRASH
static void stop_threads(int);
# endif /* HAVE_LIBCRASH */
#endif /* HAVE_PTHREAD */
static int square_valid(square_t *, cellval_t);
static int square_trivially_equiv(const square_t *, const square_t *);

/* This needs to be global, since square_solve_recursive() will use it
   to determine whether it has already solved a square which is
   trivially equivalent to the current one. Note that this is
   perfectly thread-safe because not only will threads only access it
   when `solutions_mutex' is locked, but they won't ever write to it
   anyway (only read from it)! */
static square_list_t *beginning = NULL;

/* Solve a magic square and return 0 on success and put all of the
   solutions in `*solutions' (which will be allocated) if `solutions'
   is not NULL. Store the size in `*solutions_size' if
   `solutions_size' is not NULL.

   If `keep_going' is nonzero, keep finding solutions even after the
   first match. If `file' is not NULL, print first the description of
   the square (if any) followed by each of the solutions.

   If multi-threading is enabled, use at most `max_threads' unless
   `local_auto_regulate_threads' is nonzero. If
   `local_auto_regulate_threads', automatically regulate the number of
   threads to use the CPUs most efficiently, starting at
   `max_threads'.

   Returns nonzero on error (in which case `*solutions' should NOT be
   free()'d). `*solutions' also should NOT be free()'d in the event
   that no solutions are found. Note that all the elements of
   `*solutions' should be free()'d also with square_destroy(). */
int square_solve(square_t *square, square_t **solutions,
		 size_t *solutions_size, char keep_going, char skip_trivial,
		 FILE *file, enum file_format local_format
#ifdef HAVE_PTHREAD
		 , unsigned long max_threads, char local_auto_regulate_threads
#endif /* HAVE_PTHREAD */
#ifdef HAVE_PROGRESS
		 , char local_progress,
		 const struct timespec *local_progress_interval,
		 FILE * progress_file, char *progress_filename,
		 progress_mode_t progress_mode
#endif /* HAVE_PROGRESS */
		 ) {
  square_list_t *solutions_list = NULL;
  cellval_t sum; /* The sum that all the rows, columns, and center
		    diagonals add up to */
  size_t solutions_size_local, *solutions_size_local_ptr;
  int ret
#if defined HAVE_PTHREAD && !defined NDEBUG
    , ret1
#endif
    ;
#if defined HAVE_PTHREAD && !defined HAVE_LIBCRASH
  pthread_t regulation_thread;
# ifdef HAVE_PROGRESS
  pthread_t progress_thread;
# endif /* HAVE_PROGRESS */
#endif /* HAVE_PTHREAD && !HAVE_LIBCRASH */

  /* Make the format accessible from all functions in this file. */
  format = local_format;

#if defined HAVE_PTHREAD
  /* stop_threads() needs to know whether to stop the automatic
     regulation thread. This should be set to
     `local_auto_regulate_threads', but first set it to 0 so that
     stop_threads() doesn't attempt to stop it before it's actually
     created. After it's created, we can set this to
     `local_auto_regulate_threads. */
  auto_regulate_threads	= 0;
#endif /* HAVE_PTHREAD */

#ifdef HAVE_PROGRESS
  /* Make the progress reporting information accessible from all
     functions in this file. */
  progress		= local_progress;
  progress_interval	= local_progress_interval;

  if (progress)
    progress_initz(progress_iteration);
#endif /* HAVE_PROGRESS */

  /* Make sure we have a valid file format */
  if (!(format == TEXT || format == BINARY)) {
    errno = EINVAL;
    return 1;
  }

  /* Initialize stuff */
#ifdef HAVE_PTHREAD
  threads_left = max_threads;
#endif

  solutions_size_local_ptr = solutions_size ?
    solutions_size : &solutions_size_local;

  *solutions_size_local_ptr = 0;

  write_func = (format == TEXT) ? write_human : write_machine;

  /* Get the sum */

  cellval_initz(sum);

    /* Add all of the immutable squares */
  for (size_t x = 0; x < square->size; x++) {
    for (size_t y = 0; y < square->size; y++) {
      if (!square->cells[x][y].mutable) {
	cellval_add(sum, sum, square->cells[x][y].val);

	if (!cellval_valid(sum)) {
	  /* That means an error occurred */
	  cellval_clear(sum);
	  return 1;
	}
      }
    }
  }

    /* Add the other numbers too */
  for (size_t i = 0; i < square->nums_size; i++) {
    cellval_add(sum, sum, square->nums[i]);

    if (!cellval_valid(sum)) {
      /* That means an error occurred */
      cellval_clear(sum);
      return 1;
    }
  }

  /* Divide the sum of all the numbers by the size of the square to
     get the magic sum. */
  if (cellval_int_p(sum)) {
#ifndef HAVE_LIBGMP_ALL
    char is_neg = (sum.i < 0);

    if (is_neg) {
      /* Negative integer division and modulo arithmetic does not work
	 properly for some reason when using values computed at
	 run-time. This looks like a compiler bug, but we should work
	 around it for now. */
      sum.i = -sum.i;
    }
#endif /* !HAVE_LIBGMP_ALL */

    if (!cellval_divisible_ui_p(sum, square->size)) {
      /* If the size doesn't divide evenly into the sum of all the
	 numbers, the square is impossible to solve. Think about it:
	 If there are only integers, how do you expect to get a
	 non-integer sum by adding them together? */
      cellval_clear(sum);
      return 0;
    }

#ifndef HAVE_LIBGMP_ALL
    sum.i = sum.i / square->size;

    if (is_neg)
      sum.i = -sum.i;
  }
  else {
    sum.f = sum.f / square->size;
#endif /* !HAVE_LIBGMP_ALL */
  }

#ifdef HAVE_LIBGMP_ALL
  cellval_div_ui(sum, sum, square->size);
#endif

  /* If `solutions' is NULL, we aren't saving any solutions. BUT if we
     are skipping trivially equivalent solutions, we need to save
     solutions (but we won't return them). HOWEVER, if we're only
     looking for one solution, we needn't save solutions even if we
     are skipping trival equivalence. */
  if (solutions || (skip_trivial && keep_going)) {
    /* Allocate the solutions list */
    solutions_list = mu_xmalloc(sizeof(*solutions_list));

    /* Remember that this is the end */
    solutions_list->next = NULL;

    /* Remember where the beginning is */
    beginning = solutions_list;
  }

  /* Print the description, if any and `file' isn't NULL and we're in
     text mode. */
  if (square->description && file && format == TEXT) {
    if (fputs(square->description, file) == EOF ||
	putc('\n', file) == EOF) {
      cellval_clear(sum);
      return 1;
    }

    /* We seemed to have successfully written to the file, but let's
       fflush() it just to make sure. This way, we can detect errors
       early (since fputs() and putc() may have appeared to have
       successfully written to the file, while in reality they only
       wrote to the buffer). */
    if (fflush(file)) {
      cellval_clear(sum);
      return 1;
    }
  }

#ifdef HAVE_PROGRESS
  if (progress) {
    progress_t progress_max;
# ifdef HAVE_PTHREAD
    int progress_fd = fileno(progress_file), out_fd = fileno(file);

    progress_is_output =
      progress_fd >= 0 && out_fd >= 0 && progress_fd == out_fd;
# endif /* HAVE_PTHREAD */

    progress_init_set_ui(progress_max, 1);

    /* Calculate the maximum number of possibilities. */
    for (size_t i = square->nums_size; i > 1; i--)
      progress_mul_ui(progress_max, progress_max, i);

    /* Initialize progress reporting. */
    progress_begin(progress_mode, progress_max,
		   progress_file, progress_filename);

    progress_clear(progress_max);
  }

# ifndef HAVE_PTHREAD
  /* Initialize the time of the next progress report. */
  if (clock_gettime(PROGRESS_CLOCK, &next_progress_time)) {
    cellval_clear(sum);
    return 1;
  }
# endif /* !HAVE_PTHREAD */
#endif /* HAVE_PROGRESS */
#ifdef HAVE_PTHREAD
  global_errno = 0;

  /* Initialize the mutexes for square_solve_recursive() */
  pthread_mutex_init(&solutions_mutex, NULL);
  pthread_mutex_init(&threads_left_mutex, NULL);
  pthread_mutex_init(&global_errno_mutex, NULL);
# ifdef HAVE_PROGRESS
  if (progress) {
    pthread_mutex_init(&progress_mutex, NULL);
    if (progress_is_output)
      pthread_mutex_init(&progress_file_mutex, NULL);
  }
# endif

  /* Initialize the condition variables */
  pthread_cond_init(&thread_exit, NULL);
  if (local_auto_regulate_threads)
    pthread_cond_init(&threads_left_changed, NULL);

# ifdef HAVE_PROGRESS
  /* Start the thread to display the progress bar. */
  if (progress) {
    ret = pthread_create(&progress_thread, NULL,
			 display_progress_thread, NULL);
    if (ret) {
      /* We couldn't create the thread. */
      progress = 0;
    }
  }
  else {
    ret = 0;
  }
# endif /* HAVE_PROGRESS */

# ifdef HAVE_LIBCRASH
  /* Add a hook to stop any new threads from starting if we crash. */
  crash_hook(stop_threads);
# endif

# ifdef HAVE_PROGRESS
  if (!ret) {
# endif /* HAVE_PROGRESS */
    if (local_auto_regulate_threads) {
      /* Start the auto regulation thread. */
      ret = pthread_create(&regulation_thread, NULL,
			   regulate_threads_wrapper, (void *)&max_threads);
      if (!ret) {
	/* We were able to create the thread. Now we can set
	   `auto_regulate_threads' (for stop_threads()). */
	auto_regulate_threads = local_auto_regulate_threads;
      }
    }
#endif /* HAVE_PTHREAD */
    /* Now solve the square! */
    ret = square_solve_recursive(square, sum, solutions_list ?
				 &solutions_list : NULL,
				 solutions_size_local_ptr,
				 keep_going, skip_trivial, file);

    /* We're finally done with this. */
    cellval_clear(sum);
#ifdef HAVE_PTHREAD
    pthread_mutex_lock(&threads_left_mutex);
    while (threads_left < max_threads) {
      /* There are still threads running; wait for all the threads
	 to finish. Note that pthread_cond_wait() unlocks
	 `threads_left_mutex' on entry and locks it again on
	 exit. */
# ifndef NDEBUG
      ret1 =
# endif
	pthread_cond_wait(&thread_exit, &threads_left_mutex);

      assert(!ret1);
    }
    pthread_mutex_unlock(&threads_left_mutex);
# ifdef HAVE_PROGRESS
  }
# endif
#endif /* HAVE_PTHREAD */

#ifdef HAVE_PROGRESS
  /* Finish the progress bar if it's enabled. */
  if (progress) {
# ifdef HAVE_PTHREAD
    /* Stop the progress display thread. */
    ret = pthread_cancel(progress_thread);
    if (ret) {
      pthread_mutex_lock(&global_errno_mutex);
      global_errno = ret;
      pthread_mutex_unlock(&global_errno_mutex);
    }
    else {
      /* Wait for it to finish. */
      ret = pthread_join(progress_thread, NULL);
      if (ret) {
	pthread_mutex_lock(&global_errno_mutex);
	global_errno = ret;
	pthread_mutex_unlock(&global_errno_mutex);
      }
    }
# endif /* HAVE_PTHREAD */

    progress_end();
    progress_clear(progress_iteration);
  }
#endif /* HAVE_PROGRESS */

#ifdef HAVE_PTHREAD
  if (auto_regulate_threads) {
    /* Stop the auto-regulation thread. */
    ret = pthread_cancel(regulation_thread);
    if (ret) {
      pthread_mutex_lock(&global_errno_mutex);
      global_errno = ret;
      pthread_mutex_unlock(&global_errno_mutex);
    }
    else {
      /* Wait for it to finish. */
      ret = pthread_join(regulation_thread, NULL);
      if (ret) {
	pthread_mutex_lock(&global_errno_mutex);
	global_errno = ret;
	pthread_mutex_unlock(&global_errno_mutex);
      }
    }
  }

# ifdef HAVE_LIBCRASH
  /* Reset the libcrash hook. */
  crash_hook(NULL);
# endif

  /* Destroy the mutexes */
# ifndef NDEBUG
  /* Assertions are enabled, so we should save the return value */
  ret1 =
# endif
    pthread_mutex_destroy(&solutions_mutex) STMT_SEPARATOR
    pthread_mutex_destroy(&threads_left_mutex) STMT_SEPARATOR
    pthread_mutex_destroy(&global_errno_mutex) STMT_SEPARATOR

    pthread_cond_destroy(&thread_exit);

  if (local_auto_regulate_threads) {
# ifndef NDEBUG
    ret1 = ret1 ||
# endif
      pthread_cond_destroy(&threads_left_changed);
  }

# ifdef HAVE_PROGRESS
  if (progress) {
#  ifndef NDEBUG
    ret1 = ret1 ||
#  endif /* NDEBUG */
      pthread_mutex_destroy(&progress_mutex);

    if (progress_is_output) {
#  ifndef NDEBUG
      ret1 = ret1 ||
#  endif /* NDEBUG */
	pthread_mutex_destroy(&progress_file_mutex);
    }
  }
# endif /* HAVE_PROGRESS */

  /* If we failed to destroy any of the mutexes, it means we forgot to
     unlock them first which indicates a bug. */
  assert(!ret1);
#endif /* HAVE_PTHREAD */

  if (ret) {
    /* An error occured */
    if (solutions_list) {
      while (beginning != solutions_list) {
	square_list_t *old = beginning;

	beginning = beginning->next;

	square_destroy(&(old->square));
	free(old);
      }

      free(solutions_list);
    }

#ifdef HAVE_PTHREAD
    /* `errno' is most likely not set properly since it is
       thread-local. We have to set it from `global_errno'. */
    errno = global_errno;
#endif

    return 1;
  }

  if (solutions_list) {
    if (*solutions_size_local_ptr) {
      /* Convert the list to an array if we are expected to, otherwise
	 just free() the array. */

      if (solutions)
	*solutions =
	  mu_xmalloc(sizeof(**solutions) * *solutions_size_local_ptr);

      for (size_t i = 0; i < *solutions_size_local_ptr; i++) {
	square_list_t *old = beginning;

	if (solutions)
	  (*solutions)[i] = beginning->square;
	else
	  square_destroy(&(beginning->square));

	beginning = beginning->next;

	free(old);
      }
    }

    assert(beginning == solutions_list);

    free(solutions_list);
  }

  return 0;
}

/* Duplicate a square including the description */
square_t square_dup(square_t square) {
  square_t new_square = square_dup_nodesc(square);

  if (square.description)
    new_square.description = mu_xstrdup(square.description);

  return new_square;
}

/* Duplicate a square excluding the description */
square_t square_dup_nodesc(square_t square) {
  square_t new_square;

  new_square.size		= square.size;
  new_square.nums_size		= square.nums_size;
  new_square.has_equiv_nums	= square.has_equiv_nums;
  /* Don't duplicate it; that's for square_dup() */
  new_square.description	= NULL;

  new_square.cells	= mu_xmalloc(sizeof(*(new_square.cells)) *
				     new_square.size);
  if (new_square.nums_size)
    new_square.nums	= mu_xmalloc(sizeof(*(new_square.nums)) *
				     new_square.nums_size);

  for (size_t x = 0; x < new_square.size; x++) {
    new_square.cells[x] = mu_xmalloc(sizeof(*(new_square.cells[x])) *
				     new_square.size);

    for (size_t y = 0; y < new_square.size; y++) {
      new_square.cells[x][y].mutable = square.cells[x][y].mutable;

      /* We only care about copying immutable values. */
      if (!square.cells[x][y].mutable) {
	cell_init(new_square.cells[x][y]);
	cell_set(new_square.cells[x][y], square.cells[x][y]);
      }
    }
  }

  for (size_t i = 0; i < new_square.nums_size; i++) {
    cellval_init(new_square.nums[i]);
    cellval_set(new_square.nums[i], square.nums[i]);
  }

  return new_square;
}

/* Destroy a square */
void square_destroy(square_t *square) {
  if (square->description)
    free(square->description);

  /* Free each of the cells. */
  for (size_t x = 0; x < square->size; x++) {
#ifdef HAVE_LIBGMP_ALL
    /* We need to free each cell separately. */
    for (size_t y = 0; y < square->size; y++) {
      /* Only free it if it's immutable. */
      if (!square->cells[x][y].mutable)
	cell_clear(square->cells[x][y]);
    }
#endif

    free(square->cells[x]);
  }

  /* Now free `square->cells' itself. */
  free(square->cells);

  /* Free the other numbers if there are any. */
#ifdef HAVE_LIBGMP_ALL
  /* We need to free each number separately. */
  for (size_t i = 0; i < square->nums_size; i++)
    cellval_clear(square->nums[i]);
#endif

  if (square->nums_size)
    free(square->nums);
}

/* Check if a square has multiple copies of the same number in
   `other_nums'. If it does, set `has_equiv_nums' to a nonzero value
   and return nonzero. If it doesn't, set `has_equiv_nums' to zero and
   return zero. */
int square_has_equiv_nums(square_t *square) {
  /* Check if there are multiple copies of the same number. */
  for (size_t i = 1; i < square->nums_size; i++) {
    for (size_t j = 0; j < i; j++) {
      if (cellval_equal(square->nums[i], square->nums[j]))
	return square->has_equiv_nums = 1;
    }
  }

  /* If we're here, all the numbers were different. */
  return square->has_equiv_nums = 0;
}

#ifndef HAVE_LIBGMP_ALL
/* Add two `cellval_t's and return the result. */
cellval_t __cellval_add(cellval_t a, cellval_t b) {
  /* Make sure the types are valid */
  if (!cellval_valid(a)) {
    errno = EINVAL;
    return a;
  }

  if (!cellval_valid(b)) {
    errno = EINVAL;
    return b;
  }

  switch (a.type) {
  case INT:
    switch (b.type) {
    case INT:
      a.i += b.i;
      break;
    case FLOAT:
      a.type = FLOAT;
      a.f = a.i + b.f;
      break;
    }

    break;
  case FLOAT:
    a.f += (b.type == INT) ? b.i : b.f;
    break;
  }

  return a;
}
#endif /* !HAVE_LIBGMP_ALL */

/***************************\
|* Static helper functions *|
\***************************/

/* Solve a square adding the solutions (if found) to
   `solutions'. Returns 0 on success or nonzero on error. */
static int square_solve_recursive(square_t *square, cellval_t sum,
				  square_list_t **solutions,
				  size_t *solutions_size, char keep_going,
				  char skip_trivial, FILE *file) {
  char all_immutable = 1; /* Whether all of the cells are immutable */
  int valid;
  cellval_t nums[square->nums_size];

#ifdef HAVE_PTHREAD
  pthread_mutex_lock(&global_errno_mutex);

  if (global_errno) {
    /* Something bad happened */
    pthread_mutex_unlock(&global_errno_mutex);

    return 1;
  }

  pthread_mutex_unlock(&global_errno_mutex);

  pthread_mutex_lock(&solutions_mutex);
#endif /* HAVE_PTHREAD */

  if (!keep_going && *solutions_size > 0) {
    /* We've already found a solution and we weren't asked to find
       more. */
#ifdef HAVE_PTHREAD
    pthread_mutex_unlock(&solutions_mutex);
#endif

    return 0;
  }

#ifdef HAVE_PTHREAD
  pthread_mutex_unlock(&solutions_mutex);
#endif

  valid = square_valid(square, sum);

  if (valid < 0) {
    /* Error */

#ifdef HAVE_PTHREAD
    pthread_mutex_lock(&global_errno_mutex);
    global_errno = errno;
    pthread_mutex_unlock(&global_errno_mutex);
#endif

    return 1;
  }

  /* If we do have pthreads, we don't need to do the following because
     a separate thread does it for us. */
#if defined HAVE_PROGRESS && !defined HAVE_PTHREAD
  if (progress) {
    struct timespec cur_time;

    /* Get the current time. */
    if (clock_gettime(PROGRESS_CLOCK, &cur_time))
      return 1;

    /* Check if it's time to display the progress bar. */
    if (cur_time.tv_sec > next_progress_time.tv_sec ||
	(cur_time.tv_sec == next_progress_time.tv_sec &&
	 cur_time.tv_nsec > next_progress_time.tv_nsec)) {
      /* It sure is! Display it (but don't fail if we couldn't). */
      progress_display(progress_iteration);

      /* Now set `next_progress_time' for the next time around. */
      next_progress_time.tv_sec	+= progress_interval->tv_sec;
      next_progress_time.tv_nsec	+= progress_interval->tv_nsec;
      if (next_progress_time.tv_nsec > 1000000000) {
	next_progress_time.tv_sec++;
	next_progress_time.tv_nsec -= 1000000000;
      }
    }
  }
#endif /* HAVE_PROGRESS && !HAVE_PTHREAD */

  if (!valid) {
    /* We needn't check this square; it's already invalid */

#ifdef HAVE_PROGRESS
    if (progress) {
      progress_t missed;

      progress_init_set_ui(missed, 1);

      /* Calculate how many possibilities we missed. */
      for (size_t i = square->nums_size; i > 1; i--)
	progress_mul_ui(missed, missed, i);

# ifdef HAVE_PTHREAD
      pthread_mutex_lock(&progress_mutex);
# endif

      progress_add(progress_iteration, progress_iteration, missed);

      progress_clear(missed);

# ifdef HAVE_PTHREAD
      pthread_mutex_unlock(&progress_mutex);
# endif
    }
#endif /* HAVE_PROGRESS */

    return 0;
  }

#ifdef HAVE_PROGRESS
  if (progress) {
# ifdef HAVE_PTHREAD
    pthread_mutex_lock(&progress_mutex);
# endif
    progress_add_ui(progress_iteration, progress_iteration, 1);
# ifdef HAVE_PTHREAD
    pthread_mutex_unlock(&progress_mutex);
# endif
  }
#endif /* HAVE_PROGRESS */

  /* Make a copy of `square->nums' so we can delete certain
     numbers. Note: this is a shallow copy even when libgmp is
     enabled. */
  for (size_t i = 0; i < square->nums_size; i++)
    nums[i] = square->nums[i];

  for (size_t x = 0; x < square->size && all_immutable; x++) {
    for (size_t y = 0; y < square->size && all_immutable; y++) {
      if (square->cells[x][y].mutable) {
	if (!square->nums_size) {
	  /* That's an error */
	  errno = EINVAL;

#ifdef HAVE_PTHREAD
	  pthread_mutex_lock(&global_errno_mutex);
	  global_errno = errno;
	  pthread_mutex_unlock(&global_errno_mutex);
#endif

	  return 1;
	}

	all_immutable = 0;

	/* Temporarily make the cell immutable */
	square->cells[x][y].mutable = 0;

	/* Decrement `nums_size' by one since we will delete one
	   number from `square->nums' */
	square->nums_size--;

	/* Try to solve the square with this cell replaced with each
	   of the other numbers (remember, `square->nums_size + 1' is
	   the actual size of `nums') */
	for (size_t i = 0; i < square->nums_size + 1; i++) {
	  /* Remember if the square had equivalent numbers. */
	  char has_equiv_nums = square->has_equiv_nums;

	  if (has_equiv_nums) {
	    char already_tried = 0;

	    /* Check if we've already tried this number since the square
	       has multiple numbers with the same value. */
	    for (int j = 0; j < i; j++) {
	      if (cellval_equal(nums[i], nums[j])) {
		already_tried = 1;
		break;
	      }
	    }

	    /* Don't try the number again if we've already tried it. */
	    if (already_tried) {
#ifdef HAVE_PROGRESS
	      if (progress) {
		/* Since we won't be checking this number. */
# ifdef HAVE_PTHREAD
		pthread_mutex_lock(&progress_mutex);
# endif
		progress_add_ui(progress_iteration, progress_iteration, 1);
# ifdef HAVE_PTHREAD
		pthread_mutex_unlock(&progress_mutex);
# endif
	      }
#endif /* HAVE_PROGRESS */

	      continue;
	    }
	  }

	  /* Replace the cell with the number we want to try. */
	  square->cells[x][y].val = nums[i];

	  /* Delete the number from `square->nums' */
	  square->nums[i] = square->nums[square->nums_size];

	  /* Check if there are still multiple copies of the same
	     number if there were in the first place. */
	  if (has_equiv_nums)
	    square_has_equiv_nums(square);

	  /* Try to solve it */

#ifdef HAVE_PTHREAD
	  pthread_mutex_lock(&threads_left_mutex);

	  if (threads_left) {
	    int ret = square_solve_new_thread(square, sum, solutions,
					      solutions_size, keep_going,
					      skip_trivial, file);

	    pthread_mutex_unlock(&threads_left_mutex);

	    if (ret == EAGAIN) {
	      /* We've reached the limit; don't start a new thread
		 after all. */
	      ret = square_solve_recursive(square, sum, solutions,
					   solutions_size, keep_going,
					   skip_trivial, file);
	    }

	    if (ret) {
	      /* We couldn't create a new thread for some other reason
		 than EAGAIN. */
	      square->nums[i] = nums[i];
	      square->nums_size++;
	      square->cells[x][y].mutable = 1;

	      pthread_mutex_lock(&global_errno_mutex);
	      global_errno = errno;
	      pthread_mutex_unlock(&global_errno_mutex);

	      return 1;
	    }
	  }
	  else {
	    pthread_mutex_unlock(&threads_left_mutex);
#endif /* HAVE_PTHREAD */

	    if (square_solve_recursive(square, sum, solutions,
				       solutions_size, keep_going,
				       skip_trivial, file)) {
	      /* Error */
	      square->nums[i] = nums[i];
	      square->nums_size++;
	      square->cells[x][y].mutable = 1;

#ifdef PTHREAD
	      /* No need to set `global_errno' since the recursive
		 call to square_solve_recursive() already set it. */
#endif

	      return 1;
	    }
#ifdef HAVE_PTHREAD
	  }
#endif

	  /* Put the number back */
	  square->nums[i] = nums[i];

	  /* If the square had multiple copies of the same number
	     before, it does again now. */
	  square->has_equiv_nums = has_equiv_nums;
	}

	/* And don't forget to reset `square->nums_size'! */
	square->nums_size++;

	/* Reset the cell to mutable */
	square->cells[x][y].mutable = 1;
      }
    }
  }

  if (all_immutable) {
    /* Hooray! We've found a solution! */

#ifdef HAVE_PTHREAD
    pthread_mutex_lock(&solutions_mutex);

    /* Check if we've already found a solution and we're not supposed
       to keep going again, since another thread might have found a
       solution when we weren't looking. */
    if (!keep_going && *solutions_size > 0) {
      pthread_mutex_unlock(&solutions_mutex);

      return 0;
    }
#endif

    if (solutions) {
      if (skip_trivial) {
	square_list_t *cur_solution = beginning;

	/* Make sure we don't already have a square which is trivially
	   equivalent (it is rotated/reflected or equivalent). */
	while (cur_solution->next) {
	  if (square_trivially_equiv(square, &(cur_solution->square))) {
#ifdef HAVE_PTHREAD
	    pthread_mutex_unlock(&solutions_mutex);
#endif

	    return 0;
	  }

	  cur_solution = cur_solution->next;
	}

	assert(cur_solution == *solutions);
      }

      /* Record the solution */
      (*solutions)->square = square_dup_nodesc(*square);
      (*solutions)->next = mu_xmalloc(sizeof(*((*solutions)->next)));
      (*solutions) = (*solutions)->next;
      (*solutions)->next = NULL; /* Remember that this is the end */
    }

    if (*solutions_size == SIZE_MAX) {
      /* Overflow */
      errno = EOVERFLOW;

#ifdef HAVE_PTHREAD
      pthread_mutex_lock(&global_errno_mutex);
      global_errno = errno;
      pthread_mutex_unlock(&global_errno_mutex);

      pthread_mutex_unlock(&solutions_mutex);
#endif

      return 1;
    }

    (*solutions_size)++;

    if (file) {
      char *description = square->description;

      /* Write the solution to `file' */

#if defined HAVE_PROGRESS && defined HAVE_PTHREAD
      if (progress && progress_is_output) {
	/* This is so we don't write the solution at the same time as
	   we write the progress indicator. */
	pthread_mutex_lock(&progress_file_mutex);
      }
#endif /* HAVE_PTHREAD && HAVE_PTHREAD */

      if ((*solutions_size > 1 || description) && format == TEXT) {
	/* Separate this solution from the last solution or the
	   description */
	if (putc('\n', file) == EOF) {
#ifdef HAVE_PTHREAD
	  pthread_mutex_lock(&global_errno_mutex);
	  global_errno = errno;
	  pthread_mutex_unlock(&global_errno_mutex);

	  pthread_mutex_unlock(&solutions_mutex);

# ifdef HAVE_PROGRESS
	  if (progress && progress_is_output)
	    pthread_mutex_unlock(&progress_file_mutex);
# endif /* HAVE_PROGRESS */
#endif /* HAVE_PTHREAD */

	  return 1;
	}
      }

      if (keep_going && format == TEXT) {
	if (fprintf(file, "Solution %zu:\n\n", *solutions_size) < 0) {
#ifdef HAVE_PTHREAD
	  pthread_mutex_lock(&global_errno_mutex);
	  global_errno = errno;
	  pthread_mutex_unlock(&global_errno_mutex);

	  pthread_mutex_unlock(&solutions_mutex);

# ifdef HAVE_PROGRESS
	  if (progress && progress_is_output)
	    pthread_mutex_unlock(&progress_file_mutex);
# endif /* HAVE_PROGRESS */
#endif /* HAVE_PTHREAD */

	  return 1;
	}
      }

      /* If the format is BINARY, we didn't print the description, so
	 write it in the first (and only the first) solution. */
      if (format == TEXT || *solutions_size > 1) {
	/* Temporarily get rid of the description */
	square->description = NULL;
      }

      if (!write_func(square, file) ||
	  /* Flush the stream after each solution is found */
	  fflush(file) == EOF) {
	/* Error! */
	square->description = description;

#ifdef HAVE_PTHREAD
	pthread_mutex_lock(&global_errno_mutex);
	global_errno = errno;
	pthread_mutex_unlock(&global_errno_mutex);

	pthread_mutex_unlock(&solutions_mutex);

# ifdef HAVE_PROGRESS
	  if (progress && progress_is_output)
	    pthread_mutex_unlock(&progress_file_mutex);
# endif /* HAVE_PROGRESS */
#endif /* HAVE_PTHREAD */

	return 1;
      }

#if defined HAVE_PROGRESS && defined HAVE_PTHREAD
      if (progress && progress_is_output)
	pthread_mutex_unlock(&progress_file_mutex);
#endif

      /* Replace the description */
      square->description = description;
    }

#ifdef HAVE_PTHREAD
    pthread_mutex_unlock(&solutions_mutex);
#endif
  }

  return 0;
}

#ifdef HAVE_PTHREAD
/* Create a new thread in which to solve a square. Returns zero on
   success, `errno' on error. The calling function should lock the
   mutex `threads_left_mutex' before calling and unlock it after
   completion. */
static int square_solve_new_thread(square_t *square, cellval_t sum,
				   square_list_t **solutions,
				   size_t *solutions_size, char keep_going,
				   char skip_trivial, FILE *file) {
  square_t *square_copy;
  struct solve_args *args;
  int ret, ret1;
  pthread_t thread;
  pthread_attr_t attr;

  if (!threads_left) {
    errno = EAGAIN;
    return errno;
  }

  /* Initialize the thread attributes structure. */
  ret = pthread_attr_init(&attr);

  if (ret) {
    errno = ret;
    return ret;
  }

  /* Make the thread detached so that we don't have to call
     pthread_join() on it. */
  ret = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

  if (ret) {
    errno = ret;

    ret = pthread_attr_destroy(&attr);

    if (ret)
      errno = ret;

    return errno;
  }

  /* Make a copy of the square since we will be creating a new
     thread. We also need to mu_xmalloc() it because when we die, our
     stack will be destroyed. */
  square_copy = mu_xmalloc(sizeof(*square_copy));
  *square_copy = square_dup_nodesc(*square);

  /* No need to copy the description since we won't be writing to
     it. */
  square_copy->description = square->description;

  /* We have to allocate this with mu_xmalloc because our stack will
     be destroyed. */
  args = mu_xmalloc(sizeof(*args));

  /* Pack up the arguments */
  args->square		= square_copy;
  cellval_init(args->sum);
  cellval_set(args->sum, sum);
  args->solutions	= solutions;
  args->solutions_size	= solutions_size;
  args->keep_going	= keep_going;
  args->skip_trivial	= skip_trivial;
  args->file		= file;

  /* Now start the thread */
  ret = pthread_create(&thread, &attr, square_solve_unpack_args, args);

  /* We're done with `attr' now */
  ret1 = pthread_attr_destroy(&attr);

  if (ret || ret1) {
    square_copy->description = NULL;
    square_destroy(square_copy);
    free(square_copy);

    errno = ret ? ret : ret1;
    return errno;
  }

  threads_left--;

  if (auto_regulate_threads) {
    /* Tell the auto-regulation thread that `threads_left' changed. */
    ret = pthread_cond_signal(&threads_left_changed);
  }

  assert(!ret);

  return 0;
}

/* Unpack the arguments `args' and call square_solve_recursive(). Note
   that the return value is void * to be compatible with the pthreads
   API, but it should actually be casted to a long. Returns 0 on
   success, nonzero on error. */
static void * square_solve_unpack_args(void *ptr) {
  struct solve_args args = *(struct solve_args *)ptr;
  void *ret;
# ifndef NDEBUG
  int ret1;
# endif

  free(ptr);

  ret = (void *)(long)square_solve_recursive(args.square, args.sum,
					     args.solutions,
					     args.solutions_size,
					     args.keep_going,
					     args.skip_trivial, args.file);

  /* free() the square since we made a copy */
  args.square->description = NULL;
  square_destroy(args.square);
  free(args.square);

  /* We also made a copy of `sum' if HAVE_LIBGMP_ALL is defined (if
     it's not, the following line is a no-op). */
  cellval_clear(args.sum);

  pthread_mutex_lock(&threads_left_mutex);

  threads_left++;

  /* Tell the main thread that we're done. */
  if (auto_regulate_threads) {
    /* We need to use pthread_cond_broadcast() because the
       auto-regulation thread will be listening on `thread_exit' as
       well as the main thread. */
# ifndef NDEBUG
    ret1 =
# endif
      pthread_cond_broadcast(&thread_exit) STMT_SEPARATOR
      /* And tell the auto-regulation thread that `threads_left'
	 changed. */
      pthread_cond_signal(&threads_left_changed);
  }
  else {
# ifndef NDEBUG
    ret1 =
# endif
      pthread_cond_signal(&thread_exit);
  }

  assert(!ret1);

  pthread_mutex_unlock(&threads_left_mutex);

  return ret;
}

/* Cast `*argptr' to a `struct tryunlock_arg' called `arg' and make
   sure `arg->mutex' is unlocked if `arg->do_unlock' is nonzero. It
   may currently be locked or unlocked, but after calling this
   function, it will be guaranteed to be unlocked (if `arg->do_unlock'
   is nonzero). `arg' is never changed, but is not declared as `const'
   because it wouldn't do much good since pthread_cleanup_push() does
   not declare its argument as `const'. */
static void tryunlock_mutex(void *argptr) {
  struct tryunlock_arg arg = *(struct tryunlock_arg *)argptr;

  if (!arg.do_unlock) {
    /* We don't need to do anything. */
    return;
  }

  /* Try to lock `arg->mutex' since it might actually not be locked, and it
     is an error to try to unlock a mutex which is not locked in the
     first place. */
  pthread_mutex_trylock(arg.mutex);

  /* Now, whether or not pthread_mutex_trylock() succeeded,
     `arg->mutex' is locked and we can safely unlock it. */
  pthread_mutex_unlock(arg.mutex);
}

/* A wrapper to call regulate_threads() with `arg' casted to an
   (unsigned long *). Also do some others stuff before we call
   regulate_threads() and after it returns. Returns NULL on success,
   nonzero cast to a (void *) on error, in which case `global_errno'
   will be set. */
static void * regulate_threads_wrapper(void *arg) {
  struct tryunlock_arg unlock_threads_left_mutex =
    { .mutex = &threads_left_mutex, .do_unlock = 1 };
  int ret = 0;

  /* regulate_threads() needs /proc/stat to get CPU info. */
  proc_stat = fopen("/proc/stat", "r");
  if (!proc_stat) {
    pthread_mutex_lock(&global_errno_mutex);
    global_errno = errno;
    pthread_mutex_unlock(&global_errno_mutex);

    return (void *)1;
  }

  pthread_cleanup_push(close_proc_stat, NULL);

  /* We should not buffer any information since /proc/stat is
     constantly changing. */
  if (setvbuf(proc_stat, NULL, _IONBF, 0)) {
    pthread_mutex_lock(&global_errno_mutex);
    global_errno = errno;
    pthread_mutex_unlock(&global_errno_mutex);

    /* Use pthread_exit() instead of return so that we call the
       cleanup handler. */
    pthread_exit((void *)1);
  }

  pthread_cleanup_push(tryunlock_mutex, &unlock_threads_left_mutex);

  /* Begin regulating the number of threads. */
  regulate_threads((unsigned long *)arg);

  /* If regulate_threads() returns, that is an error. */
  pthread_mutex_lock(&global_errno_mutex);
  global_errno = errno;
  pthread_mutex_unlock(&global_errno_mutex);

  ret = 1;

  pthread_cleanup_pop(0);
  pthread_cleanup_pop(1);

  return (void *)(long)ret;
}

/* Close `proc_stat'. `arg' is unused. On error, `global_errno' will
   be set. */
void close_proc_stat(void *arg) {
  if (fclose(proc_stat)) {
    pthread_mutex_lock(&global_errno_mutex);
    global_errno = errno;
    pthread_mutex_unlock(&global_errno_mutex);
  }
}

/* Regulate the number of threads starting at `max_threads' to utilize
   the CPUs most efficiently, blocking until all threads are
   finished. If the CPUs are utilized 100%, we will start to decrease
   threads until the CPUs are not utilized completely, at which point
   we will start to increase threads again until they are utilized
   completely again. `threads_left_mutex' should be locked before
   calling and unlocked after completion. Does not return on success
   (parent thread must be canceled), returns on error. */
static void regulate_threads(unsigned long *max_threads) {
  const struct timespec sleeptime = { 0, 100000000 }; /* 1/10 sec */
  int ret;
  /* The optimum number of threads. */
  struct {
    unsigned long threads;
    int performance;
  } optimum = { 0, -1 };

  pthread_mutex_lock(&threads_left_mutex);

  /* Wait for the first thread to start if it hasn't already. */
  while (threads_left >= *max_threads) {
    ret = pthread_cond_wait(&threads_left_changed, &threads_left_mutex);

    if (ret) {
      errno = ret;
      pthread_mutex_unlock(&threads_left_mutex);

      return;
    }
  }

  while (1) {
    int cpu_usage;

    /* Wait until the maximum number of threads are running so we can
       get an accurate assessment of efficiency. */
    while (threads_left && threads_left < *max_threads) {
      ret = pthread_cond_wait(&threads_left_changed, &threads_left_mutex);

      if (ret) {
	errno = ret;
	pthread_mutex_unlock(&threads_left_mutex);

	return;
      }
    }

    pthread_mutex_unlock(&threads_left_mutex);

    /* Get the cpu usage */
    cpu_usage = get_cpu_usage(&sleeptime);

    if (cpu_usage < 0) {
      /* An error occurred. */
      return;
    }

    pthread_mutex_lock(&threads_left_mutex);

    if (cpu_usage == optimum.performance) {
      if (*max_threads < optimum.threads) {
	/* It's better to have fewer threads if they perform the
	   same. */
	optimum.threads = *max_threads;
      }
      else if (*max_threads > optimum.threads) {
	/* Like I said, it's better to have fewer threads if they
	   perform the same. So reduce the number of threads. */
	for (unsigned long i = 0; i < *max_threads - optimum.threads; i++) {
	  while (!threads_left) {
	    ret = pthread_cond_wait(&thread_exit, &threads_left_mutex);

	    if (ret) {
	      errno = ret;
	      pthread_mutex_unlock(&threads_left_mutex);

	      return;
	    }
	  }

	  threads_left--;
	}

	*max_threads = optimum.threads;
      }
      else if (cpu_usage < CPU_USAGE_MAX) {
	/* We can do better! */
	(*max_threads)++;
	threads_left++;
      }
      else if (*max_threads > 1) {
	/* We're using the maximum CPU. That's good! But keep the
	   number of threads in check. */
	while (!threads_left) {
	  ret = pthread_cond_wait(&thread_exit, &threads_left_mutex);

	  if (ret) {
	    errno = ret;
	    pthread_mutex_unlock(&threads_left_mutex);

	    return;
	  }
	}

	(*max_threads)--;
	threads_left--;
      }
    }
    else if (cpu_usage > optimum.performance) {
      /* We've found a number of threads that performs even better. */
      optimum.threads = *max_threads;
      optimum.performance = cpu_usage;
    }
    else {
      /* Try to get closer to the optimum number of threads. */
      if (*max_threads == optimum.threads) {
	/* We're already at the supposed optimum number of threads but
	   that doesn't appear to be accurate anymore, so make the
	   current performance the optimum performance and try a few
	   more threads. */
	optimum.performance = cpu_usage;

	(*max_threads)++;
	threads_left++;
      }
      else if (*max_threads < optimum.threads) {
	/* We need to allow more threads. */
	(*max_threads)++;
	threads_left++;
      }
      /* Only decrease the number if it's greater than one (we need at
	 least one thread running). */
      else if (*max_threads > 1) {
	/* Wait for a thread to exit. */
	while (!threads_left) {
	  ret = pthread_cond_wait(&thread_exit, &threads_left_mutex);

	  if (ret) {
	    errno = ret;
	    pthread_mutex_unlock(&threads_left_mutex);

	    return;
	  }
	}

	/* Don't allow so many threads. */
	(*max_threads)--;
	threads_left--;
      }
    }
  }
}

/* Get the total CPU usage from now to `interval' in the
   future. Returns CPU usage times CPU_USAGE_MAX on success, negative
   on error. */
static int get_cpu_usage(const struct timespec *interval) {
  long work_time_1, work_time_2, total_time_1, total_time_2;

  /* Get the CPU times. */
  if (get_cpu_times(&work_time_1, &total_time_1))
    return -1;

  /* Add work time to idle time to get the total. */
  total_time_1 += work_time_1;

  /* `total_time_1' cannot be equal to `total_time_2'. If they are,
     get the cpu times again and continue doing that until they're
     not. */
  do {
    /* Sleep for the time interval. */
    if (nanosleep(interval, NULL))
      return -1;

    /* Get the CPU times again. */
    if (get_cpu_times(&work_time_2, &total_time_2))
      return -1;

    total_time_2 += work_time_2;
  } while (total_time_1 == total_time_2);

  /* Return the usage. Note that we have to multiply by CPU_USAGE_MAX
     *before* we divide so that it isn't floored too early. */
  return ((work_time_2 - work_time_1) * CPU_USAGE_MAX) /
    (total_time_2 - total_time_1);
}

/* Get CPU time spent working and idle and store it in `work' and
   `idle'. Returns 0 on success, nonzero on error.

   WARNING: This function is expensive since it parses /proc/stat. I/O
   shouldn't be a problem since /proc is a virtual filesystem, but the
   parsing is what's expensive. I don't know of any other way to get
   the CPU usage, though. */
static int get_cpu_times(long *work, long *idle) {
  long work_times[7] = { 0 }, idle_times[2] = { 0 };
  int ret;

  /* Read the first line of /proc/stat to get CPU information. We
     ignore stolen time. */
  rewind(proc_stat);
  ret = fscanf(proc_stat, "cpu %ld %ld %ld %ld %ld %ld %ld %*d %ld %ld",
	       work_times, work_times + 1, work_times + 2, idle_times,
	       idle_times + 1, work_times + 3, work_times + 4,
	       /* ignored, */ work_times + 5, work_times + 6);

  /* Values after `idle' (`idle_times[0]') are only available on Linux
     2.5.41 or later (see proc(5)), so don't panic if we can't read
     those (but do panic if we couldn't even read those). */
  if (ret < 4) {
    /* EOF is guaranteed to be < 0 */
    if (ret != EOF)
      errno = ENOMSG;

    return 1;
  }

  /* Add up all the work times */
  *work = 0;
  for (int i = 0; i < sizeof(work_times) / sizeof(*work_times); i++)
    *work += work_times[i];

  /* Add up all the idle times */
  *idle = 0;
  for (int i = 0; i < sizeof(idle_times) / sizeof(*idle_times); i++)
    *idle += idle_times[i];

  return 0;
}

# ifdef HAVE_PROGRESS
/* Display progress continuously. Should be passed to
   pthread_create(). `arg' is unused. Returns NULL on success, nonzero
   cast to a (void *) on error, in which case `global_errno' will be
   set. */
void * display_progress_thread(void *arg) {
  struct timespec cur_time;
  struct timespec next_time;
  struct tryunlock_arg unlock_progress_file_mutex =
    { .mutex = &progress_file_mutex,
      .do_unlock = progress_is_output };

  /* Make sure we unlock `progress_file_mutex' when we are
     canceled. */
  pthread_cleanup_push(tryunlock_mutex, &unlock_progress_file_mutex);

  /* Get the current time. */
  if (clock_gettime(PROGRESS_CLOCK, &cur_time)) {
    pthread_mutex_lock(&global_errno_mutex);
    global_errno = errno;
    pthread_mutex_unlock(&global_errno_mutex);

    return (void *)1;
  }

  next_time = cur_time;

  while (1) {
    int ret;

    /* Sleep until we have to display the progress again. */
    do {
      ret = clock_nanosleep(PROGRESS_CLOCK, TIMER_ABSTIME, &next_time, NULL);
    } while (ret == EINTR);

    if (ret) {
      pthread_mutex_lock(&global_errno_mutex);
      global_errno = errno;
      pthread_mutex_unlock(&global_errno_mutex);

      return (void *)1;
    }

    /* Display the progress (but don't fail if we couldn't). We
       shouldn't need to lock `progress_mutex' since we're only
       reading `progress_iteration', not writing. However, if the
       progress output file is the same as the solutions output file,
       we need to lock `progress_file_mutex' so we don't write the
       progress indicator at the same time as a solution. */
    if (progress_is_output)
      pthread_mutex_lock(&progress_file_mutex);

    progress_display(progress_iteration);

    if (progress_is_output)
      pthread_mutex_unlock(&progress_file_mutex);

    /* Now set `next_time' for the next time around. */
    next_time.tv_sec	+= progress_interval->tv_sec;
    next_time.tv_nsec	+= progress_interval->tv_nsec;
    if (next_time.tv_nsec > 1000000000) {
      next_time.tv_sec++;
      next_time.tv_nsec -= 1000000000;
    }
  }

  pthread_cleanup_pop(0);

  return NULL;
}
# endif /* HAVE_PROGRESS */

# ifdef HAVE_LIBCRASH
/* A hook to run if we crash. */
static void stop_threads(int signum) {
  /* Lock `threads_left_mutex' forever so that no new threads will
     start (and thread regulation will halt). */
  pthread_mutex_lock(&threads_left_mutex);
  /* Also make sure no existing threads will print anything. */
  pthread_mutex_lock(&solutions_mutex);

#  ifdef HAVE_PROGRESS
  if (progress) {
    /* Stop the progress display thread. */
    pthread_cancel(progress_thread);
  }
#  endif /* HAVE_PROGRESS */
  if (auto_regulate_threads) {
    /* Stop the regulation thread. */
    pthread_cancel(regulation_thread);
  }
}
# endif /* HAVE_LIBCRASH */
#endif /* HAVE_PTHREAD */

/* Check if a square is valid, returning 1 if it is, 0 if it's not,
   and -1 on error. */
static int square_valid(square_t *square, cellval_t sum) {
  char mutable; /* Whether at least one cell was mutable */
  cellval_t check_sum;

  if (!cellval_valid(sum)) {
    errno = EINVAL;
    return -1;
  }

  cellval_init(check_sum);

  /* Check each column */
  for (size_t x = 0; x < square->size; x++) {
    cellval_set_ui(check_sum, 0);
    mutable = 0;

    for (size_t y = 0; y < square->size; y++) {
      if (square->cells[x][y].mutable) {
	mutable = 1;
	break;
      }

      cellval_add(check_sum, check_sum, square->cells[x][y].val);

      if (!cellval_valid(check_sum)) {
	cellval_clear(check_sum);
	return -1;
      }
    }

    if (!mutable && !cellval_equal(check_sum, sum)) {
      /* It's invalid */
      cellval_clear(check_sum);
      return 0;
    }
  }

  /* Check each row */
  for (size_t y = 0; y < square->size; y++) {
    cellval_set_ui(check_sum, 0);
    mutable = 0;

    for (size_t x = 0; x < square->size; x++) {
      if (square->cells[x][y].mutable) {
	mutable = 1;
	break;
      }

      cellval_add(check_sum, check_sum, square->cells[x][y].val);

      if (!cellval_valid(check_sum)) {
	cellval_clear(check_sum);
	return -1;
      }
    }

    if (!mutable && !cellval_equal(check_sum, sum)) {
      /* It's invalid */
      cellval_clear(check_sum);
      return 0;
    }
  }

  /* Check the upper left to lower right diagonal */
  cellval_set_ui(check_sum, 0);
  mutable = 0;

  for (size_t i = 0; i < square->size; i++) {
    if (square->cells[i][i].mutable) {
      mutable = 1;
      break;
    }

    cellval_add(check_sum, check_sum, square->cells[i][i].val);

    if (!cellval_valid(check_sum)) {
      cellval_clear(check_sum);
      return -1;
    }
  }

  if (!mutable && !cellval_equal(check_sum, sum)) {
    /* It's invalid */
    cellval_clear(check_sum);
    return 0;
  }

  /* Check the upper right to lower left diagonal */
  cellval_set_ui(check_sum, 0);
  mutable = 0;

  for (size_t i = 0; i < square->size; i++) {
    if (square->cells[square->size - 1 - i][i].mutable) {
      mutable = 1;
      break;
    }

    cellval_add(check_sum, check_sum,
		square->cells[square->size - 1 - i][i].val);

    if (!cellval_valid(check_sum)) {
      cellval_clear(check_sum);
      return -1;
    }
  }

  if (!mutable && !cellval_equal(check_sum, sum)) {
    /* It's invalid */
    cellval_clear(check_sum);
    return 0;
  }

  cellval_clear(check_sum);

  /* If we're here, the square is valid */
  return 1;
}

/* Check whether `a' is trivially equivalent to `b'; i.e. it is a
   rotation/reflection of `b' or is equivalent to `b'. Returns
   positive if they are trivially equivalent, zero if they are
   non-trivially different, and negative if an error occured. */
static int square_trivially_equiv(const square_t *a, const square_t *b) {
  enum transform { REF_HORIZ, REF_VERT, REF_DIAG_UL_LR, REF_DIAG_UR_LL,
		   ROT_90, ROT_180, ROT_270, ROT_0, TRANS_MAX };

  /* The squares must be the same size and they must be complete (no
     `other_nums'). */
  if (a->size != b->size || a->nums_size || b->nums_size) {
    errno = EINVAL;
    return -1;
  }

  for (enum transform trans = 0; trans < TRANS_MAX; trans++) {
    char equiv = 1; /* Whether the two squares are equivalent */

    for (size_t x = 0; x < a->size && equiv; x++) {
      for (size_t y = 0; y < a->size && equiv; y++) {
	cellval_t comp_cell; /* The cell to compare against */

	/* Make sure the current cell in `b' is valid */
	if (!cellval_valid(b->cells[x][y].val)) {
	  errno = EINVAL;
	  return -1;
	}

	/* Transform `a' and get the cell to compare against. */
	switch (trans) {
	case REF_HORIZ:
	  /* Horizontal reflection */
	  comp_cell = a->cells[a->size - 1 - x][y].val;
	  break;
	case REF_VERT:
	  /* Vertical reflection */
	  comp_cell = a->cells[x][a->size - 1 - y].val;
	  break;
	case REF_DIAG_UL_LR:
	  /* Diagonal reflection (from upper left corner to lower
	     right corner) */
	  comp_cell = a->cells[a->size - 1 - y][a->size - 1 - x].val;
	  break;
	case REF_DIAG_UR_LL:
	  /* Diagonal reflection (from upper right corner to lower
	     left corner) */
	  comp_cell = a->cells[y][x].val;
	  break;
	case ROT_90:
	  /* 90 degree rotation */
	  comp_cell = a->cells[a->size - 1 - y][x].val;
	  break;
	case ROT_180:
	  /* 180 degree rotation */
	  comp_cell = a->cells[a->size - 1 - x][a->size - 1 - y].val;
	  break;
	case ROT_270:
	  /* 270 degree rotation */
	  comp_cell = a->cells[y][a->size - 1 - x].val;
	  break;
	case ROT_0:
	  /* 0 degree rotation (no transformation) */
	  comp_cell = a->cells[x][y].val;
	  break;
	default:
	  assert(0);
	}

	/* Make sure `comp_cell' is valid */
	if (!cellval_valid(comp_cell)) {
	  errno = EINVAL;
	  return -1;
	}

	/* Now check if they're equal */
	if (!cellval_equal(comp_cell, b->cells[x][y].val)) {
	  /* They're not equal which means the whole squares with this
	     transformation are not equivalent. */
	  equiv = 0;
	}
      }
    }

    if (equiv) {
      /* The squares are equivalent. We're done! */
      return 1;
    }
  }

  /* If we're here, the squares were not equivalent. */
  return 0;
}
