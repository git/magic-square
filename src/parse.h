/************************************************************************\
 * Magic Square solves magic squares.                                   *
 * Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>           *
 *                                                                      *
 * This file is part of Magic Square.                                   *
 *                                                                      *
 * Magic Square is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * Magic Square is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with Magic Square.  If not, see                                *
 * <https://www.gnu.org/licenses/>.                                     *
\************************************************************************/

/* parse.h -- parse human-readable and machine-readable magic square
   files */

#ifndef _PARSE_H
#define _PARSE_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>

#include "square.h"

/* Definitions of parse characters */
#define HLINE '-'
#define VLINE '|'
#define JOINT '+'

typedef struct char_list {
  struct char_list *prev, *next;
  char c;
} clist_t;

int parse_machine(square_t *, FILE *);
int parse_human(square_t *, FILE *);

#endif /* !_PARSE_H */
