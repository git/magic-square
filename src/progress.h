/************************************************************************\
 * Magic Square solves magic squares.                                   *
 * Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>           *
 *                                                                      *
 * This file is part of Magic Square.                                   *
 *                                                                      *
 * Magic Square is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * Magic Square is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with Magic Square.  If not, see                                *
 * <https://www.gnu.org/licenses/>.                                     *
\************************************************************************/

/* progress.h -- functions for displaying progress bars */

#ifndef _PROGRESS_H
#define _PROGRESS_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#ifdef HAVE_LIBGMP_PROGRESS
# include <gmp.h>

# define progress_init(num)		mpz_init(num)
# define progress_initz(num)		mpz_init(num)
# define progress_init_set(num, n)	mpz_init_set(num, n)
# define progress_init_set_ui(num, n)	mpz_init_set_ui(num, n)
# define progress_init_set_si(num, n)	mpz_init_set_si(num, n)
# define progress_clear(num)		mpz_clear(num)
# define progress_set(res, num)		mpz_set(res, num)
# define progress_add(res, a, b)	mpz_add(res, a, b)
# define progress_add_ui(res, a, b)	mpz_add_ui(res, a, b)
# define progress_add_si(res, a, b)	mpz_add_si(res, a, b)
# define progress_mul(res, a, b)	mpz_mul(res, a, b)
# define progress_mul_ui(res, a, b)	mpz_mul_ui(res, a, b)
# define progress_mul_si(res, a, b)	mpz_mul_si(res, a, b)
#else /* !HAVE_LIBGMP_PROGRESS */
# define progress_init(num)
# define progress_initz(num)		((num) = 0)
# define progress_init_set(num, n)	((num) = (n))
# define progress_init_set_ui(num, n)	((num) = (n))
# define progress_init_set_si(num, n)	((num) = (n))
# define progress_clear(num)
# define progress_set(res, num)		((res) = (num))
# define progress_add(res, a, b)	((res) = (a) + (b))
# define progress_add_ui(res, a, b)	((res) = (a) + (b))
# define progress_add_si(res, a, b)	((res) = (a) + (b))
# define progress_mul(res, a, b)	((res) = (a) * (b))
# define progress_mul_ui(res, a, b)	((res) = (a) * (b))
# define progress_mul_si(res, a, b)	((res) = (a) * (b))
#endif /* !HAVE_LIBGMP_PROGRESS */

#ifdef HAVE_LIBGMP_PROGRESS
typedef mpz_t progress_t;
#else
typedef unsigned long long progress_t;
#endif

typedef enum progress_mode { ERROR, AUTO, BAR, DOT } progress_mode_t;

progress_mode_t progress_parse_mode(const char *);
int progress_begin(progress_mode_t, progress_t, FILE *, const char *);
int progress_display(progress_t);
int progress_cancel(void);
int progress_end(void);

#endif /* !_PROGRESS_H */
