/************************************************************************\
 * Magic Square solves magic squares.                                   *
 * Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>           *
 *                                                                      *
 * This file is part of Magic Square.                                   *
 *                                                                      *
 * Magic Square is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * Magic Square is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with Magic Square.  If not, see                                *
 * <https://www.gnu.org/licenses/>.                                     *
\************************************************************************/

/* square.h -- type definitions for magic squares */

#ifndef _SQUARE_H
#define _SQUARE_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stddef.h>
#include <stdint.h>
#ifdef HAVE_LIBGMP_ALL
# include <gmp.h>
#endif
#ifdef HAVE_PROGRESS
# include <time.h>

# include "progress.h"
#endif

/* The output file version */
#define FILE_VERSION 2

/* The magic number we write out when we write a binary file */
#define MAGIC_NUMBER 0x80b9faf4

/* cellval_valid() checks if a `cellval_t' is valid, cellval_equal()
   checks if two `cellval_t's are equal to each other, and the rest
   are initialization and arithmetic functions. For
   cellval_divisible_ui_p(), it must be known that `val' is an
   integer. See also the documentation for the corresponding GMP
   functions. */
#ifdef HAVE_LIBGMP_ALL
# define cellval_valid(val)		1
# define cellval_val(val)		(*(val))
# define cellval_init(val) do {			\
    (val) = malloc(sizeof(*(val)));		\
    mpq_init(*(val));				\
  } while (0)
# define cellval_initz(val)		cellval_init(val)
# define cellval_clear(val) do {		\
    mpq_clear(*(val));				\
    free(val);					\
  } while (0)
# define cellval_set(new, old)		mpq_set(*(new), *(old))
# define cellval_set_ui(val, i)		mpq_set_ui(*(val), i, 1)
# define cellval_set_si(val, i)		mpq_set_si(*(val), i, 1)
# define cellval_set_d(val, d)		mpq_set_d(*(val), d)
# define cellval_set_cellvali(val, vali) mpq_set_z(*(val), *(vali))
# define cellval_add(val, a, b)		mpq_add(*(val), *(a), *(b))
# define cellval_add_ui(val, a, b)	mpq_add_ui(*(val), *(a), b)
# define cellval_div_ui(val, a, b) do {				\
    if ((val) != (a))						\
      cellval_set(val, a);					\
    mpz_mul_ui(mpq_denref(*(val)), mpq_denref(*(val)), b);	\
    mpq_canonicalize(*(val));					\
  } while (0)
# define cellval_int_p(val)			\
  (mpz_cmp_ui(mpq_denref(*(val)), 1) == 0)
# define cellval_divisible_ui_p(val, div)	\
  mpz_divisible_ui_p(mpq_numref(*(val)), div)
# define cellval_cmp(a, b)		mpq_cmp(*(a), *(b))
# define cellval_equal(a, b)		mpq_equal(*(a), *(b))
# define cellval_lt(a, b)		(cellval_cmp(a, b) < 0)
# define cellval_gt(a, b)		(cellval_cmp(a, b) > 0)
# define cellval_cmp_ui(val, i)		mpq_cmp_ui(*(val), i, 1)
# define cellval_equal_ui(val, i)	(cellval_cmp_ui(val, i) == 0)
# define cellval_lt_ui(val, i)		(cellval_cmp_ui(val, i) < 0)
# define cellval_gt_ui(val, i)		(cellval_cmp_ui(val, i) > 0)
# define cellval_cmp_cellvali(val, vali)	\
  mpq_cmp_z(*(val), *(vali))
# define cellval_equal_cellvali(val, vali)	\
  (cellval_cmp_cellvali(val, vali) == 0)
# define cellval_lt_cellvali(val, vali)	(cellval_cmp_cellvali(val, vali) < 0)
# define cellval_gt_cellvali(val, vali)	(cellval_cmp_cellvali(val, vali) > 0)

# define cellvali_init(vali) do {		\
    (vali) = malloc(sizeof(*(vali)));		\
    mpz_init(*(vali));				\
  } while (0)
# define cellvali_initz(vali)		cellvali_init(vali)
# define cellvali_init_set_ui(vali, i) do {	\
    (vali) = malloc(sizeof(*(vali)));		\
    mpz_init_set_ui(*(vali), i);		\
  } while (0)
# define cellvali_clear(vali) do {		\
    mpz_clear(*(vali));				\
    free(vali);					\
  } while (0)
# define cellvali_set_ui(vali, i)	mpz_set_ui(*(vali), i)

# define cell_init(cell)		cellval_init((cell).val)
# define cell_clear(cell)		cellval_clear((cell).val)
# define cell_set(new, old) do {		\
    (new).mutable = (old).mutable;		\
    cellval_set((new).val, (old).val);		\
  } while (0)
#else /* !HAVE_LIBGMP_ALL */
# define cellval_valid(val)			\
  ((val).type == INT || (val).type == FLOAT)
# define cellval_val(val)			\
  (((val).type == INT) ? (val).i : (val).f)
# define cellval_init(val)
# define cellval_initz(val) do {		\
    (val).type = INT;				\
    (val).i = 0;				\
  } while (0)
# define cellval_clear(val)
# define cellval_set(new, old)		((new) = (old))
/* Use `num' here instead of `i' so as not to conflict with the `i' in
   `(val).i'. */
# define cellval_set_ui(val, num) do {		\
    (val).type = INT;				\
    (val).i = (num);				\
  } while (0)
# define cellval_set_si(val, i)		cellval_set_ui(val, i)
# define cellval_set_d(val, d) do {		\
    (val).type = FLOAT;				\
    (val).f = (d);				\
  } while (0)
# define cellval_set_cellvali(val, vali) cellval_set_ui(val, vali)
# define cellval_add(val, a, b)		(val) = __cellval_add(a, b)
# define cellval_add_ui(val, a, b) do {		\
    (val) = (a);				\
    cellval_val(val) += (b);			\
  } while (0)
# define cellval_div_ui(val, a, b)		\
  (cellval_val(val) = cellval_val(a) / (b))
# define cellval_int_p(val)		((val).type == INT)
# define cellval_divisible_ui_p(val, div)	\
  (!((val).i % div))
# define cellval_cmp(a, b)		(cellval_val(a) - cellval_val(b))
# define cellval_equal(a, b)		(cellval_val(a) == cellval_val(b))
# define cellval_lt(a, b)		(cellval_val(a) < cellval_val(b))
# define cellval_gt(a, b)		(cellval_val(a) > cellval_val(b))
# define cellval_cmp_ui(val, i)		(cellval_val(val) - i)
# define cellval_equal_ui(val, i)	(cellval_val(val) == i)
# define cellval_lt_ui(val, i)		(cellval_val(val) < i)
# define cellval_gt_ui(val, i)		(cellval_val(val) > i)
# define cellval_cmp_cellvali(val, vali)	cellval_cmp_ui(val, vali)
# define cellval_equal_cellvali(val, vali)	cellval_equal_ui(val, vali)
# define cellval_lt_cellvali(val, vali)		cellval_lt_ui(val, vali)
# define cellval_gt_cellvali(val, vali)		cellval_gt_ui(val, vali)

# define cellvali_init(vali)
# define cellvali_initz(vali)		((vali) = 0)
# define cellvali_init_set_ui(vali, i)	((vali) = (i))
# define cellvali_clear(vali)
# define cellvali_set_ui(vali, i)	((vali) = (i))

# define cell_init(cell)
# define cell_clear(cell)
# define cell_set(new, old)		((new) = (old))
#endif /* !HAVE_LIBGMP_ALL */

#ifndef HAVE_LIBGMP_ALL
/* An `int' type for cells. */
typedef long long cellvali_t;
/* And a `float' type. */
typedef long double cellvalf_t;
#else /* HAVE_LIBGMP_ALL */
/* These are used for reading binary files written with a version
   without support for GMP. */
typedef long long cellvali_nongmp_t;
typedef long double cellvalf_nongmp_t;
#endif /* HAVE_LIBGMP_ALL */

/* Define this even if we have GMP, because we need it for reading
   files with non-GMP types. */
enum cell_type { INT, FLOAT };

/* A single cell value; it should be signed since we support negative
   numbers */
#ifdef HAVE_LIBGMP_ALL
/* Use pointers so we can make shallow copies. */
typedef mpq_t *cellval_t;
typedef mpz_t *cellvali_t;
#else /* !HAVE_LIBGMP_ALL */
typedef struct cellval {
  union {
    cellvali_t i;
    cellvalf_t f;
  };
  enum cell_type type;
} cellval_t;
#endif /* !HAVE_LIBGMP_ALL */

/* A single cell */
typedef struct cell {
  cellval_t val;
  char mutable; /* Whether this cell is permitted to change */
} cell_t;

/* A magic square */
typedef struct square {
  cell_t **cells;
  cellval_t *nums;	/* Other valid numbers */
  size_t size,		/* The size of the magic square */
    nums_size; 		/* The size of `nums' */
  char has_equiv_nums;	/* Whether or not there are multiple copies of
			   the same number in `nums'. */
  char *description;	/* An optional description of the magic
			   square */
} square_t;

/* A list of magic squares */
typedef struct square_list {
  struct square_list *next;
  square_t square;
} square_list_t;

/* We have to include "write.h" down here because "write.h" includes
   us and it uses types defined above. The problem is this (assuming
   this is the first time we're included):

    1. We are included from a file that is not "write.h"
    2. We check if _SQUARE_H is defined
    3. It's not, so define _SQUARE_H and continue
    4. We include "write.h"
    5. "write.h" includes us
    6. We (included from "write.h") check if _SQUARE_H is defined
    7. It is this time (from step 3.), so don't do anything
    8. "write.h" defines functions using types defined here
    9. The compiler errors out since we (when included from write.h)
       didn't define those types
   10. If we didn't error out, we would have continued to define
       everything for the file that included us in step 1. (not
       "write.h")

   Oh, by the way: we need "write.h" to define `enum file_format' for
   us.

   The opposite would occur if "write.h" were included first
   ("write.h" includes us, we include "write.h", "write.h" dosen't
   define what we need, the compiler errors out). That is why
   "write.h" needs to define `enum file_format' before including us
   (it does). */
#include "write.h"

int square_solve(square_t *, square_t **, size_t *,
		 char, char, FILE *, enum file_format
#ifdef HAVE_PTHREAD
		 , unsigned long, char
#endif
#ifdef HAVE_PROGRESS
		 , char, const struct timespec *,
		 FILE *, char *, progress_mode_t
#endif
		 );
square_t square_dup(square_t);
square_t square_dup_nodesc(square_t);
void square_destroy(square_t *);
int square_has_equiv_nums(square_t *);
#ifndef HAVE_LIBGMP_ALL
cellval_t __cellval_add(cellval_t, cellval_t);
#endif

#endif /* !_SQUARE_H */
