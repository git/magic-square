/************************************************************************\
 * Magic Square solves magic squares.                                   *
 * Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>           *
 *                                                                      *
 * This file is part of Magic Square.                                   *
 *                                                                      *
 * Magic Square is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * Magic Square is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with Magic Square.  If not, see                                *
 * <https://www.gnu.org/licenses/>.                                     *
\************************************************************************/

/* parse.c -- parse human-readable and machine-readable magic square
   files */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <endian.h>
#include <math.h>
#include <assert.h>
#ifdef HAVE_LIBGMP_ALL
# if defined HAVE_LOCALE_H
#  include <locale.h>
# elif defined HAVE_LOCALECONV
/* Don't use localeconv() even though we have it because we don't have
   the header file. */
#  undef HAVE_LOCALECONV
# endif /* !HAVE_LOCALE_H && HAVE_LOCALECONV */
# include <gmp.h>
#endif /* HAVE_LIBGMP_ALL */
#include <mu/safe.h>

#include "square.h"
#include "parse.h"
#include "input.h"

static int file_version, have_gmp_types;

/* Static helper functions */
static int parse_machine_cellval(cellval_t *, FILE *);
static int parse_machine_cell(cell_t *, FILE *);

static clist_t ** parse_human_nodesc(square_t *, FILE *);
static int parse_human_line(square_t *, FILE *, clist_t **, char);
static int parse_human_separator_cell(FILE *, size_t, clist_t **);
static int parse_human_cells(cell_t **, clist_t **, size_t, size_t);
static int parse_human_other_nums(square_t *, FILE *, clist_t **);
#ifndef HAVE_LIBGMP_ALL
# define parse_human_cellval(cellval, str)	\
  __parse_human_cellval(&(cellval), str)
static int __parse_human_cellval(cellval_t *, const char *);
#else /* HAVE_LIBGMP_ALL */
static int parse_human_cellval(cellval_t, const char *);
static int parse_float_mpq(mpq_t, const char *, int);
#endif /* HAVE_LIBGMP_ALL */

static int fill_other_nums(square_t *);

/* Parse a machine readable binary format, returns 0 on success,
   nonzero on error */
int parse_machine(square_t *square, FILE *file) {
  const uint32_t magic_number = MAGIC_NUMBER;
  uint32_t magic_number_check;
  size_t description_size, condensed_mutable_cells_size;
  char *condensed_mutable_cells;
  size_t index = 0;
  char subindex = 0;

  /* Read the magic number */
  if (!fread(&magic_number_check, sizeof(magic_number_check), 1, file))
    return 1;

  /* Convert it from big endian to host byte order and compare it to
     the actual magic number */
  magic_number_check = be32toh(magic_number_check);
  if (magic_number_check != magic_number) {
    errno = ENOMSG;
    return 1;
  }

  /* Make sure we have the right file version number */
  if ((file_version = getc(file)) == EOF)
    return 1;

  switch (file_version) {
  case 0:
    /* We didn't use to use GMP at all. */
    have_gmp_types = 0;
    break;

  case 1:
  case FILE_VERSION:
    /* Check if we are using GMP types. */
    if ((have_gmp_types = getc(file)) == EOF)
      return 1;

    break;

  default:
    errno = ENOMSG;
    return 1;
  }

#ifndef HAVE_LIBGMP
  if (have_gmp_types) {
    /* We do not support GMP types. */
    errno = ENOTSUP;
    return 1;
  }
#endif

  /* Get the sizes of stuff */
  if (!(fread(&(square->size), sizeof(square->size), 1, file) &&
	fread(&(square->nums_size), sizeof(square->nums_size), 1, file) &&
	fread(&description_size, sizeof(description_size),
	      1, file))) {
    return 1;
  }

  /* Convert them to host byte order */
  square->size		= be64toh(square->size);
  square->nums_size	= be64toh(square->nums_size);
  description_size	= be64toh(description_size);

  if (file_version == FILE_VERSION) {
    condensed_mutable_cells_size =
      ((square->size * square->size) + 7) / 8;
    condensed_mutable_cells =
      mu_xcalloc(condensed_mutable_cells_size,
		 sizeof(*condensed_mutable_cells));

    /* Read the condensed mutable cells. */
    if (fread(condensed_mutable_cells, sizeof(*condensed_mutable_cells),
	      condensed_mutable_cells_size, file) !=
	condensed_mutable_cells_size) {
      free(condensed_mutable_cells);
      return 1;
    }
  }
  else
    condensed_mutable_cells = NULL; /* Stifle -Wmaybe-uninitialized */

  /* Allocate the cells */
  square->cells = mu_xmalloc(sizeof(*(square->cells)) * square->size);

  /* Get the cells */
  for (size_t x = 0; x < square->size; x++) {
    square->cells[x] =
      mu_xmalloc(sizeof(*(square->cells[x])) * square->size);

    for (size_t y = 0; y < square->size; y++) {
      int ret;

      if (file_version == FILE_VERSION) {
	/* Get whether this cell is mutable from the condensed
	   version. */
	square->cells[x][y].mutable =
	  condensed_mutable_cells[index] & (1 << subindex);

	if (++subindex > 7) {
	  subindex = 0;
	  index++;

	  assert(index < condensed_mutable_cells_size ||
		 (index == condensed_mutable_cells_size && !subindex));
	}
      }

      /* Now parse the actual cell. */
      ret = parse_machine_cell(&(square->cells[x][y]), file);
      if (ret) {
	/* Free the cells */
	for (size_t i = 0; i <= x; i++)
	  free(square->cells[i]);
	free(square->cells);
	free(condensed_mutable_cells);

	/* Now return an error (`errno' will already have been set, if
	   applicable) */
	return ret;
      }
    }
  }

  if (file_version == FILE_VERSION) {
    assert(index == condensed_mutable_cells_size - !!subindex);
    assert(subindex == (square->size * square->size) % 8);

    free(condensed_mutable_cells);
  }

  if (square->nums_size) {
    /* Get the other valid numbers */
    square->nums =
      mu_xmalloc(sizeof(*(square->nums)) * square->nums_size);

    for (size_t i = 0; i < square->nums_size; i++) {
      int ret;

      cellval_init(square->nums[i]);
      ret = parse_machine_cellval(&(square->nums[i]), file);

      if (ret) {
	free(square->nums);

	for (size_t j = 0; j < square->size; j++)
	  free(square->cells[j]);
	free(square->cells);

	return ret;
      }
    }
  }

  if (description_size) {
    /* And finally get the description */
    square->description = mu_xmalloc(sizeof(*(square->description)) *
				     (description_size + 1));

    if (fread(square->description, sizeof(*(square->description)),
	      description_size, file) != description_size) {
      free(square->description);
      free(square->nums);

      for (size_t i = 0; i < square->size; i++)
	free(square->cells[i]);
      free(square->cells);

      return 1;
    }

    /* And the '\0' */
    square->description[description_size] = '\0';
  }
  else {
    /* There was no description */
    square->description = NULL;
  }

  /* Check if there are multiple copies of the same number. */
  square_has_equiv_nums(square);

  return 0;
}

/* Parse a human readable format, returns 0 on success, nonzero on
   error */
int parse_human(square_t *square, FILE *file) {
  clist_t *description = mu_xmalloc(sizeof(*description));
  size_t description_size; /* The size of the description (including
			      the '\0') */

  description->prev = NULL; /* So we know where the beginning is */

  /* Parse the description and the rest of the square */
  while (1) {
    /* This could be the end of the description. We'll start parsing
       and consider it not the end if we fail. */

    /* What to append to the description if this turns out not to be
       the end; `description_append' is the first element of the
       list and `description_append + 1' is the last */
    clist_t **description_append;

    errno = 0;
    description_append = parse_human_nodesc(square, file);

    if (description_append) {
      int c;

      /* We're not done yet; keep parsing, but first add the
	 characters to the description */

      /* Since the last char of `description' will be '\0' */
      if (description->prev) {
	description = description->prev;
	free(description->next);

	description_append[0]->prev = description;
	description->next = description_append[0];
      }
      else {
	free(description);
      }

      /* Now go to the end */
      description = description_append[1];

      /* Note that we are only free()ing the ARRAY (the clist_t **),
	 not the actual LISTS (the clist_t *) */
      free(description_append);

      /* Squares can only start at the beginning of a line, so read to
	 the end of this line. */
      c = description->prev->c;

      while (c != '\n') {
	c = getinput(file, &description);

	if (c == EOF) {
	  errno = ENOMSG;
	  goto error;
	}
      }
    }
    else if (errno) {
      /* There was an error */
      goto error;
    }
    else {
      /* We're done! */
      break;
    }
  }

  /* Remove trailing whitespace */
  while (description->prev && isspace(description->prev->c)) {
    description = description->prev;
    free(description->next);
  }

  /* Set `next' to NULL so we know where the end is */
  description->next = NULL;

  /* Now convert the linked list to a string */

  /* First get the size while also rewinding to the beginning of the
     list */
  for (description_size = 0; description->prev; description_size++) {
    if (description_size == SIZE_MAX - 1 /* -1 since we will be using
                                             `description_size + 1' */) {
      errno = EOVERFLOW;

      while (description->next)
	description = description->next;

      goto error;
    }

    description = description->prev;
  }

  if (description_size) {
    /* Now allocate the string */
    square->description = mu_xmalloc(sizeof(*(square->description)) *
				     (description_size + 1));

    /* Now copy the list to the string */
    for (size_t i = 0; i < description_size; i++) {
      square->description[i] = description->c;
      description = description->next;

      /* We don't need this anymore */
      free(description->prev);
    }
    assert(!description->next);
    /* Don't forget the '\0' */
    square->description[description_size] = '\0';
  }
  else {
    /* No description */
    square->description = NULL;
  }

  free(description);

  /* The rest of `square' will already be set by
     parse_human_nodesc() */

  /* Check if there are multiple copies of the same number. */
  square_has_equiv_nums(square);

  return 0;

 error:
  /* Free the description */
  while (description->prev) {
    description = description->prev;
    free(description->next);
  }
  free(description);

  return 1;
}

/***************************\
|* Static helper functions *|
\***************************/

/* Parse a `cellval_t' from a machine readable binary file, returning
   0 on success and nonzero on error. */
static int parse_machine_cellval(cellval_t *cellval, FILE *file) {
#ifdef HAVE_LIBGMP
  if (have_gmp_types) {
    size_t num_count, den_count;
    void *data;
    int neg;
# ifdef HAVE_LIBGMP_ALL
#  define value cellval_val(*cellval)
# else
    mpq_t value;
# endif

    /* Get whether the cellval is negative. */
    if ((neg = getc(file)) == EOF)
      return 1;

    /* First get the byte count for the numerator. */
    if (!fread(&num_count, sizeof(num_count), 1, file))
      return 1;

    /* Convert to host byte order. */
    num_count = be64toh(num_count);

    /* And get the numerator itself. */
    data = mu_xmalloc(num_count);
    if (fread(data, 1, num_count, file) != num_count) {
      free(data);
      return 1;
    }

# ifndef HAVE_LIBGMP_ALL
    mpq_init(value);
# endif

    /* Convert the data into the numerator. The options used here for
       mpz_import() must be the same as the ones used for mpz_export()
       in write.c. */
    mpz_import(mpq_numref(value), num_count, 1, 1, 1, 0, data);

    /* Repeat for the denominator. */
    if (!fread(&den_count, sizeof(den_count), 1, file)) {
      free(data);
# ifndef HAVE_LIBGMP_ALL
      mpq_clear(value);
# endif
      return 1;
    }
    den_count = be64toh(den_count);

    if (den_count > num_count) {
      /* The denominator is bigger than the numerator, so we need to
	 reallocate `data'. realloc() is probably more efficient if
	 `den_count' and `num_count' do not differ very much (because
	 it won't do anything in that case), but if it does need to
	 perform a reallocation, it will copy the old data to the new
	 memory location. Instead of risking that, just manually
	 reallocate it. */
      free(data);
      data = mu_xmalloc(den_count);
    }

    if (fread(data, 1, den_count, file) != den_count) {
      free(data);
# ifndef HAVE_LIBGMP_ALL
      mpq_clear(value);
# endif
      return 1;
    }

    mpz_import(mpq_denref(value), den_count, 1, 1, 1, 0, data);

    free(data);

    /* This should be already canonicalized, but you can't be too
       sure. */
    mpq_canonicalize(value);

    if (neg)
      mpq_neg(value, value);

# ifdef HAVE_LIBGMP_ALL
#  undef value
# else /* !HAVE_LIBGMP_ALL */
    /* Convert the `mpq_t' to a `cellval_t'. If it can be represented
       as an integer, convert it to an integer if it fits, otherwise
       to a double. If it cannot be represented as an integer, convert
       it to a double. */
    if (mpz_cmp_ui(mpq_denref(value), 1) == 0) {
      if (mpz_fits_slong_p(mpq_numref(value)))
	cellval_set_si(*cellval, mpz_get_si(mpq_numref(value)));
      else
	cellval_set_d(*cellval, mpz_get_d(mpq_numref(value)));
    }
    else {
      cellval_set_d(*cellval, mpq_get_d(value));
    }

    /* We're done with this. */
    mpq_clear(value);

    /* Make sure the resulting `cellval_t' is finite. */
    if (cellval->type == FLOAT && !isfinite(cellval->f)) {
      errno = ERANGE;
      return 1;
    }
# endif /* !HAVE_LIBGMP_ALL */
  }
  else {
#endif /* HAVE_LIBGMP */
    enum cell_type type;

    /* Get the type */
    if ((type = getc(file)) == EOF)
      return 1;

#ifndef HAVE_LIBGMP_ALL
    cellval->type = type;
#endif

    switch (type) {
#ifdef HAVE_LIBGMP_ALL
      cellvali_nongmp_t cellvali;
      cellvalf_nongmp_t cellvalf;
      double cellval_double;
#else /* !HAVE_LIBGMP_ALL */
# define cellvali (cellval->i)
# define cellvalf (cellval->f)
#endif /* !HAVE_LIBGMP_ALL */

    case INT:
      if (!fread(&cellvali, sizeof(cellvali), 1, file))
	return 1;

      /* Convert to host byte order */
      cellvali = be64toh(cellvali);

#ifdef HAVE_LIBGMP_ALL
      cellval_set_si(*cellval, cellvali);
#endif

      break;
    case FLOAT:
      if (!fread(&cellvalf, sizeof(cellvalf), 1, file))
	return 1;

      /* TODO: There is no handling for byte order or other
	 portability issues for floating point numbers; fix it. */

      /* Make sure the number is finite. */
      if (!isfinite(cellvalf)) {
	errno = ERANGE;
	return 1;
      }

#ifdef HAVE_LIBGMP_ALL
      cellval_double = (double)cellvalf;

      /* cellval_set_d (which uses mpq_set_d()) takes a double as an
	 argument. Even if `cellvalf' is finite, casting it to a
	 double might make it infinite (since `cellvalf' is usually
	 longer than a double). */
      if (!isfinite(cellval_double)) {
	errno = ERANGE;
	return 1;
      }

      cellval_set_d(*cellval, cellval_double);
#endif

      break;
    default:
      errno = ENOMSG;
      return 1;
    }
#ifdef HAVE_LIBGMP
  }
#else
# undef cellvali
# undef cellvalf
#endif

  return 0;
}

/* Parse a cell from a machine readable binary file, returning 0 on
   success and nonzero on error. */
static int parse_machine_cell(cell_t *cell, FILE *file) {
  int ret;

  /* In older versions, we would write the mutability along with the
     cell. */
  if (file_version == 1) {
    /* Get the mutability. */
    if ((ret = getc(file)) == EOF)
      return 1;

    cell->mutable = ret;
  }

  /* Parse the value of the cell, but only if it's immutable or if we
     can't get the mutability yet. */
  if (file_version == 0 || !cell->mutable) {
    cellval_init(cell->val);
    if ((ret = parse_machine_cellval(&(cell->val), file)))
      return ret;
  }

  /* In really old versions, we would write the mutability after the
     cell. */
  if (file_version == 0) {
    /* Now, we can get the mutability. */
    if ((ret = getc(file)) == EOF)
      return 1;

    cell->mutable = ret;

    if (cell->mutable) {
      /* Turns out we didn't need the value after all. */
      cellval_clear(cell->val);
    }
  }

  return 0;
}

/* Parse a magic square assuming the description was already parsed,
   returning NULL if successful or we've reached EOF, otherwise
   returning two pointers to a linked list of characters that were
   read, the first pointer at the first position and the second
   pointer at the last. */
static clist_t ** parse_human_nodesc(square_t *square, FILE *file) {
  char *prespace = NULL; /* Whitespace prepended before each line */
  clist_t /* What to return if we fail */
    **return_lists = mu_xmalloc(sizeof(*return_lists) * 2),
    *return_list_pos; /* A certain position in `return_lists' */
  size_t prespace_size = 1; /* 1 for the '\0' */
  int c, ret;

  return_lists[0] = mu_xmalloc(sizeof(*(return_lists[0])));

  /* Remember the first position */
  return_lists[0]->prev = NULL; /* So we know where the beginning is */
  return_lists[1] = return_lists[0];

  /* Allow each line to be prepended with spaces and tabs */
  do {
    c = getinput(file, return_lists + 1);
  } while (strchr(" \t", c));

  if (c == EOF) {
    /* EOF to soon! */
    errno = ENOMSG;
    goto error;
  }

  /* Unget `c' so parse_human_line() can parse it */
  ungetinput(c, file, return_lists + 1);

  return_lists[1]->next = NULL; /* So we know where the end is */

  for (return_list_pos = return_lists[1];
       return_list_pos->prev; prespace_size++) {
    if (prespace_size == SIZE_MAX) {
      errno = EOVERFLOW;
      goto error;
    }

    return_list_pos = return_list_pos->prev;
  }

  assert(return_list_pos == return_lists[0]);

  /* Allocate the string */
  prespace = mu_xmalloc(sizeof(*prespace) * prespace_size);

  /* Copy the list to the string */
  for (size_t i = 0; i < prespace_size - 1; i++) {
    prespace[i] = return_list_pos->c;
    return_list_pos = return_list_pos->next;
  }
  assert(!return_list_pos->next);
  /* Don't forget the '\0' */
  prespace[prespace_size - 1] = '\0';

  /* Parse the line */
  ret = parse_human_line(square, file, return_lists + 1, 0);

  if (ret > 1)
    /* Parse error */
    goto parse_error;

  if (ret < 0)
    /* Some other error; `errno' should be set */
    goto error;

  /* We shouldn't be done parsing on the first line */
  assert(!ret);

  /* Read whitespace up to a newline */
  while ((c = getinput(file, return_lists + 1)) != '\n') {
    if (c == EOF) {
      /* EOF too soon */
      errno = ENOMSG;
      goto error;
    }

    if (!isspace(c))
      /* Invalid char */
      goto parse_error;
  }

  /* Now parse lines until we're done */
  do {
    /* Skip the prepended whitespace */
    for (size_t i = 0; i < prespace_size - 1; i++) {
      c = getinput(file, return_lists + 1);

      if (c == EOF) {
	errno = ENOMSG;
	goto error;
      }

      if (c != prespace[i])
	goto parse_error;
    }

    /* Parse the line */
    ret = parse_human_line(square, file, return_lists + 1, 0);

    if (!ret) {
      /* Read whitespace up to a newline */
      while ((c = getinput(file, return_lists + 1)) != '\n') {
	if (c == EOF) {
	  /* EOF too soon */
	  errno = ENOMSG;
	  goto error;
	}

	if (!isspace(c))
	  /* Invalid char */
	  goto parse_error;
      }
    }
  } while (!ret);

  if (ret > 1)
    /* Parse error */
    goto parse_error;

  if (ret < 0)
    /* Some other error; `errno' should be set */
    goto error;

  /* Now parse the line which has the rest of the valid numbers not
     already in the square */
  ret = parse_human_other_nums(square, file, return_lists + 1);

  if (ret) {
    /* Free the cells in `square' */
    for (size_t i = 0; i < square->size; i++)
      free(square->cells[i]);
    free(square->cells);

    if (ret > 0)
      /* Parse error */
      goto parse_error;

    if (ret < 0)
      /* Some other error; `errno' should be set */
      goto error;
  }

  /* We're done parsing! */
  goto success;

 parse_error:
  if (prespace)
    free(prespace);

  /* Reset parse_human_line()'s internal variables */
  parse_human_line(NULL, NULL, NULL, 1);

  return return_lists;

 error:
  /* `errno' is assumed to be set (otherwise it will look like we
     succeeded in parsing) */
  assert(errno);

 success:
  if (prespace)
    free(prespace);

  /* Free the return lists */
  while (return_lists[1]->prev) {
    return_lists[1] = return_lists[1]->prev;
    free(return_lists[1]->next);
  }

  assert(return_lists[1] == return_lists[0]);

  free(return_lists[1]);
  free(return_lists);

  /* Reset parse_human_line()'s internal variables */
  parse_human_line(NULL, NULL, NULL, 1);

  return NULL;
}

/* Parse a single line of a file; `*parsed_chars' will be the
   characters read from the file. If `reset', reset all internal
   variables. Returns 0 on success, 1 if done parsing, > 1 on parse
   error, and negative (with `errno' set) on some other error. */
static int parse_human_line(square_t *square, FILE *file,
			    clist_t **parsed_chars, char reset) {
  /* Internal static variables */
  static size_t
    cell_width  = 0, cell_height = 0,
    square_size = 0;
  static size_t
    cell_row   = 0, /* The cell row we are currently parsing */
    square_row = 0; /* The row on the square we are currently parsing */
  static char separator = 1; /* Whether the next line will be a separator */
  static clist_t **cell_strings = NULL; /* The strings in the cells
					   for the row we are
					   currently parsing */
  static cell_t **cells = NULL; /* The known cells */

  int c, ret;

  if (reset) {
    /* Reset our internal static variables */
    if (cell_strings) {
      for (size_t i = 0; i < square_size; i++) {
	while (cell_strings[i]->prev) {
	  cell_strings[i] = cell_strings[i]->prev;
	  free(cell_strings[i]->next);
	}

	free(cell_strings[i]);
      }

      free(cell_strings);
      cell_strings = NULL;
    }

    if (cells) {
      for (size_t x = 0; x < square_size; x++) {
#ifdef HAVE_LIBGMP_ALL
	for (size_t y = 0; y < square_size; y++) {
	  if (!cells[x][y].mutable)
	    cell_clear(cells[x][y]);
	}
#endif

	free(cells[x]);
      }

      free(cells);
      cells = NULL;
    }

    cell_width  = 0; cell_height = 0;
    square_size = 0;

    cell_row   = 0;
    square_row = 0;

    separator = 1;

    return 0;
  }

  /* If we know the square size, we should know the cell width
     too. And if we don't know the square size, then we don't know the
     cell width either (yet). */
  assert(!square_size == !cell_width);

  /* Get the first input chararter */
  c = getinput(file, parsed_chars);

  if (c == EOF) {
    /* EOF too soon */
    errno = ENOMSG;
    return -1;
  }

  if (!separator && !cell_height && c == JOINT) {
    /* Well, well, well! Whad'ya know? It should have been a separator
       after all! */
    separator = 1;

    /* And now we know the cell_height */
    cell_height = cell_row;

    if (!cell_height) {
      /* We can't have 0 cell height */
      ungetinput(c, file, parsed_chars); /* So we can parse it again */
      return 2;
    }

    /* We should also parse the cells we got */

    /* Allocate `cells' if it's not already */
    if (!cells) {
      cells = mu_xmalloc(sizeof(*cells) * square_size);

      for (size_t x = 0; x < square_size; x++)
	cells[x] = mu_xmalloc(sizeof(*(cells[x])) * square_size);
    }

    /* Now parse the cells */
    ret = parse_human_cells(cells, cell_strings, square_size, square_row);

    if (ret)
      return ret;

    /* And we're on the next row */
    square_row++;
  }

  if (separator) {
    /* The first character should be a joint */
    if (c != JOINT)
      return 2;

    if (square_size) {
      /* Reset `cell_row' since we're starting a new row of cells */
      cell_row = 0;

      for (size_t parsed_cells = 0;
	   parsed_cells < square_size; parsed_cells++) {
	ret = parse_human_separator_cell(file, cell_width, parsed_chars);

	if (ret) {
	  if (ret == 1) {
	    if (parsed_cells < square_size -1)
	      /* We shouldn't be done yet */
	      return 2;
	  }
	  else {
	    /* Some other error */
	    return ret;
	  }
	}
      }
    }
    else {
      /* We don't know the square size, and we don't know the cell
	 width yet either */

      /* First find the cell width */
      while ((c = getinput(file, parsed_chars)) == HLINE)
	cell_width++;

      if (c == EOF) {
	/* EOF too soon */
	errno = ENOMSG;
	return -1;
      }

      if (!cell_width)
	/* Cannot have 0 cell width */
	return 2;

      /* The terminating character should be a joint */
      if (c != JOINT)
	return 2;

      square_size = 1;

      /* Check if we're not already done with the line */
      if (!isspace(peekinput(file))) {
	/* Find the square size by parsing the rest of the line */
	while (!(ret =
		 parse_human_separator_cell(file, cell_width,
					    parsed_chars))) {
	  square_size++;
	}

	if (ret != 1)
	  return ret;

	/* Add one for the last cell */
	square_size++;
      }

      /* Now that we know the square size, we can allocate
	 `cell_strings' */
      cell_strings = mu_xmalloc(sizeof(*cell_strings) * square_size);

      for (size_t i = 0; i < square_size; i++) {
	cell_strings[i] = mu_xmalloc(sizeof(*(cell_strings[i])));

	/* So we know where the beginning is */
	cell_strings[i]->prev = NULL;
      }
    }

    if (square_row == square_size) {
      /* We're done! */

      /* Commit the stuff we parsed to `square' */
      square->size  = square_size;
      square->cells = cells;

      /* So reset doesn't try to free `cells' (since it is now in
	 `square') */
      cells = NULL;

      return 1;
    }

    /* The next line won't be a separator since this one was */
    separator = 0;
  }
  else {
    /* If it's not a separator, we should know the cell width and
       square size (although not necessarily the cell height) */
    assert(cell_width && square_size);

    /* The first character should be a vertical line */
    if (c != VLINE)
      return 2;

    /* Now parse each of the cells */
    for (size_t i = 0; i < square_size; i++) {
      /* Parse the cell */
      for (size_t j = 0; j < cell_width; j++) {
	c = getinput(file, parsed_chars);

	if (c == EOF) {
	  /* EOF too soon */
	  errno = ENOMSG;
	  return -1;
	}

	if (c != ' ') {
	  /* Add it to the characters for this cell */
	  cell_strings[i]->c = c;
	  cell_strings[i]->next =
	    mu_xmalloc(sizeof(*(cell_strings[i]->next)));
	  cell_strings[i]->next->prev = cell_strings[i];
	  cell_strings[i] = cell_strings[i]->next;
	}
      }

      /* The terminating character should be a vertical line */
      c = getinput(file, parsed_chars);

      if (c == EOF) {
	/* EOF too soon */
	errno = ENOMSG;
	return -1;
      }

      if (c != VLINE)
	return 2;
    }

    /* Increment the cell row counter and check if it's equal to the
       cell height */
    if (++cell_row == cell_height) {
      /* Parse the strings into integers */
      ret = parse_human_cells(cells, cell_strings, square_size, square_row);

      if (ret)
	return ret;

      /* The next row should be a separator */
      separator = 1;

      /* Increment the row counter for the row on the square we are
	 currently parsing */
      square_row++;
    }
  }

  return 0;
}

/* Parse the separator part of a cell, for example: "---+" */
static int parse_human_separator_cell(FILE *file, size_t cell_width,
				      clist_t **parsed_chars) {
  int c;

  /* Parse the first part of the cell */
  for (size_t parsed_hlines = 0;
       parsed_hlines < cell_width; parsed_hlines++) {
    c = getinput(file, parsed_chars);

    if (c == EOF) {
      /* EOF too soon */
      errno = ENOMSG;
      return -1;
    }

    if (c != HLINE)
      return 2;
  }

  /* And the terminating joint */
  c = getinput(file, parsed_chars);

  if (c == EOF) {
    /* EOF too soon */
    errno = ENOMSG;
    return -1;
  }

  if (c != JOINT)
    return 2;

  return isspace(peekinput(file)) ?
    1 :	/* We're done parsing */
    0;	/* We're done parsing the cell, but not the line */
}

/* Parse cell strings into integers */
static int parse_human_cells(cell_t **cells, clist_t **cell_strings,
		       size_t square_size, size_t row) {
  for (size_t i = 0; i < square_size; i++) {
    char *string;
    size_t size = 0;

    /* Turn the character list into a conventional string */

    /* So we can check where the end is */
    cell_strings[i]->next = NULL;

    /* First get the size and rewind to the beginning */
    while (cell_strings[i]->prev) {
      cell_strings[i] = cell_strings[i]->prev;
      size++;
    }

    if (size) {
      int errno_save, ret;

      /* Now allocate the string */
      string =
	mu_xmalloc(sizeof(*string) * (size + 1 /* 1 for the '\0' */));

      /* Now copy the list to the string */
      for (size_t j = 0; j < size; j++) {
	string[j] = cell_strings[i]->c;
	cell_strings[i] = cell_strings[i]->next;
	free(cell_strings[i]->prev); /* We don't need that anymore */
      }

      assert(!cell_strings[i]->next);

      /* So it will can reset properly */
      cell_strings[i]->prev = NULL;

      /* Don't forget the '\0' */
      string[size] = '\0';

      /* Parse the string */
      cell_init(cells[i][row]);
      errno = 0;
      ret = parse_human_cellval(cells[i][row].val, string);
      errno_save = errno;

      free(string);

      if (ret) {
	/* Error */

	       /*-----------------*\-<----<----<----<----<--\
*/	return (!errno_save * 3) - 1;/*			    |
	Sorry, but I can't resist clever little things like /.
	It's equivalent to `errno_save' ? -1 : 2'. */
      }

      /* The value was specified, so mark it as immutable. */
      cells[i][row].mutable = 0;
    }
    else {
      /* The value was not specified, so mark it as mutable. */
      cells[i][row].mutable = 1;
    }
  }

  return 0;
}

/* Parse the line which has the other valid numbers; return value > 0
   is parse error, < 0 is other error and 0 is success */
static int parse_human_other_nums(square_t *square, FILE *file,
				  clist_t **parsed_chars) {
  int c;
  size_t max_size = square->size * square->size;
  cellval_t raw_nums[max_size]; /* Before we process them */
  size_t raw_nums_size = 0;

  /* Calculate the size of the final array */
  square->nums_size = 0;
  for (size_t x = 0; x < square->size; x++) {
    for (size_t y = 0; y < square->size; y++) {
      if (square->cells[x][y].mutable)
	square->nums_size++;
    }
  }

  /* Skip whitespace */
  do {
    c = getinput(file, parsed_chars);
  } while (isspace(c));

  if (c == EOF) {
#ifndef NDEBUG
    int ret;
#endif

    /* Try to default to a square with 1, 2, 3, etc. */
    if ((
#ifndef NDEBUG
	 ret =
#endif
	 fill_other_nums(square))) {
      assert(ret > 0);

      /* We couldn't guess the other numbers. :( */
      errno = ENOMSG;
      return -1;
    }

    /* Either the square already has all the numbers or we were able
       to guess them. */
    return 0;
  }

  /* The first character should be a '(' */
  if (c != '(') {
#ifndef NDEBUG
    int ret;
#endif

    if ((
#ifndef NDEBUG
	 ret =
#endif
	 fill_other_nums(square))) {
      assert(ret > 0);

      return 1;
    }

    /* It's OK */
    ungetinput(c, file, parsed_chars);
    return 0;
  }

  /* Now parse the numbers */
  while (raw_nums_size < max_size) {
    /* The beginning of where we start parsing the stuff */
    clist_t *beginning;
    char *string;
    size_t string_size = 0;
    int ret;

    /* Skip whitespace */
    do {
      c = getinput(file, parsed_chars);
    } while (isspace(c));

    if (c == EOF) {
      errno = ENOMSG;
      return -1;
    }

    if (!raw_nums_size && c == ')') {
      /* It's empty */

#ifndef NDEBUG
      int ret;
#endif

      if ((
#ifndef NDEBUG
	   ret =
#endif
	   fill_other_nums(square))) {
	assert(ret > 0);

	return 1;
      }

      return 0;
    }

    beginning = (*parsed_chars)->prev;

    /* Parse until whitespace */
    while (!isspace(c) && !strchr("),", c)) {
      c = getinput(file, parsed_chars);
      string_size++;
    }

    /* Now allocate the string and copy the parsed characters to it */
    string = mu_xmalloc(sizeof(*string) * (string_size + 1));

    for (size_t i = 0; i < string_size; i++) {
      string[i] = beginning->c;
      beginning = beginning->next;
    }

    assert(beginning == (*parsed_chars)->prev);

    /* Don't forget the '\0' */
    string[string_size] = '\0';

    /* And parse the string */
    cellval_init(raw_nums[raw_nums_size]);
    ret = parse_human_cellval(raw_nums[raw_nums_size], string);

    free(string);

    if (ret) {
      /* Parse error */
      cellval_clear(raw_nums[raw_nums_size]);
      return 1;
    }

    raw_nums_size++;

    if (c == ')')
      /* We're done */
      break;

    /* Skip whitespace */
    while (isspace(c)) {
      c = getinput(file, parsed_chars);
    }

    if (c == EOF) {
      errno = ENOMSG;
      return -1;
    }

    if (c == ')')
      /* We're done */
      break;

    if (c != ',') {
      /* Parse error */
      return 1;
    }
  }

  if (c != ')') {
    /* Parse error */
    return 1;
  }

  if (raw_nums_size != square->nums_size) {
    if (raw_nums_size == max_size) {
      /* We need to process them */

      size_t remove_nums_size = max_size - square->nums_size,
	i = 0;
      cellval_t remove_nums[remove_nums_size];

      for (size_t x = 0; x < square->size; x++) {
	for (size_t y = 0; y < square->size; y++) {
	  if (!square->cells[x][y].mutable) {
	    assert(i < remove_nums_size);

	    remove_nums[i++] = square->cells[x][y].val;
	  }
	}
      }

      assert(i == remove_nums_size);

      /* Remove the numbers */
      for (i = 0; i < remove_nums_size; i++) {
	char found = 0;

	/* Search for the number and remove it */
	for (size_t j = 0; j < raw_nums_size; j++) {
	  if (cellval_equal(raw_nums[j], remove_nums[i])) {
	    /* Remove it */
	    cellval_clear(raw_nums[j]);
	    raw_nums[j] = raw_nums[--raw_nums_size];
	    found = 1;
	    break;
	  }
	}

	if (!found) {
	  /* We couldn't find it; that's a parse error */
	  return 1;
	}
      }

      assert(raw_nums_size == square->nums_size);
    }
    else {
      /* Parse error */
      return 1;
    }
  }

  /* If there are any numbers to copy over, allocate the array and
     copy them over. Otherwise, we needn't even allocate it. */
  if (square->nums_size) {
    square->nums =
      mu_xmalloc(sizeof(*(square->nums)) * square->nums_size);

    for (size_t i = 0; i < square->nums_size; i++) {
      square->nums[i] = raw_nums[i];
    }
  }

  return 0;
}

/* Parse a string representation of a `cellval_t' into
   `cellval'. Returns 0 on success, nonzero on error in which case the
   contents of `cellval' will be undefined. */
#ifndef HAVE_LIBGMP_ALL
static int __parse_human_cellval(cellval_t *cellval, const char *str) {
  int errno_save = errno; /* So we can reset it later */
  char *endptr;

  errno = 0;
  cellval->i = strtoll(str, &endptr, 0);

  if (errno) {
    /* Couldn't parse as an int? Try as a float instead. */
    errno = 0;
    cellval->f = strtold(str, &endptr);

    if (errno || *endptr)
      return -1;

    cellval->type = FLOAT;
  }
  else if (*endptr) {
    /* Check if it looks like a fraction. */
    while (*endptr && isspace(*endptr))
      endptr++;

    if (*endptr == '/') {
      char *denstr = endptr + 1;
      cellvali_t den;

      /* Parse the denominator. */
      errno = 0;
      den = strtoll(denstr, &endptr, 0);

      if (errno || *endptr) {
	/* Fine. Lets just try parsing it as a float. */
	errno = 0;
	cellval->f = strtold(str, &endptr);

	if (errno || *endptr)
	  return -1;

	cellval->type = FLOAT;
      }
      else {
	/* Hey! It worked! */
	if (cellval->i % den) {
	  /* Now lets just divide the numerator by the denominator... */
	  cellval->f = (cellvalf_t)cellval->i / (cellvalf_t)den;
	  cellval->type = FLOAT;
	}
	else {
	  /* It can be represented in an integer. */
	  cellval->i /= den;
	  cellval->type = INT;
	}
      }
    }
    else {
      /* OK. It's not a fraction. Let's try parsing it as a float
	 then. */
      errno = 0;
      cellval->f = strtold(str, &endptr);

      if (errno || *endptr)
	return -1;

      cellval->type = FLOAT;
    }
  }
  else {
    /* We were able to parse it as a simple integer. */
    cellval->type = INT;
  }

  if (cellval->type == FLOAT && !isfinite(cellval->f)) {
    /* If it's infinite or NaN, set `errno' to ERANGE. */
    errno = ERANGE;
    return -1;
  }

  if (!errno)
    /* Reset errno */
    errno = errno_save;

  return 0;
}
#else /* HAVE_LIBGMP_ALL */
static int parse_human_cellval(cellval_t cellval, const char *str) {
  /* First try to parse it as a rational number. */
  if (mpq_set_str(cellval_val(cellval), str, 0)) {
    /* Couldn't parse it as a rational number? Try parsing it as a
       float instead. */
    if (parse_float_mpq(cellval_val(cellval), str, 0)) {
      /* Couldn't parse it as a float either? Too bad. */
      return -1;
    }
  }
  else {
    /* mpq_set_str() succeeded, but it does not canonicalize its
       result. We need to do that manually. */
    mpq_canonicalize(cellval_val(cellval));
  }

  /* Well, something must have worked if we're here. */
  return 0;
}

/* Parse a floating point string (such as "5.3") into an `mpq_t'
   rational number. Whitespace is ignored for compatibility with other
   GMP functions. */
static int parse_float_mpq(mpq_t res, const char *const_str, int base) {
  char *str, *str_beg;
  size_t str_size;
  char *exp_str;
  /* The base to raise to the exponent. This is equal to `base' for
     'e' and '@' exponents and 2 for 'p' conversion. */
  int exp_base = 0;
  mpz_t before_decimal, multiplier;
  mpq_t after_decimal;
  char decimal_point;
  char *after_decimal_str;
  char neg = 0, neg_exp = 0;

  if (base && (base < 2 || base > 62))
    return -1;

# ifdef HAVE_LOCALECONV
  /* Get the decimal point for the current locale. */
  decimal_point = *(localeconv()->decimal_point);
# else
  decimal_point = '.';
# endif

  /* Copy `const_str' because we are not allowed to change
     `const_str'. */
  str = mu_xstrdup(const_str);

  /* Remember the beginning of `str' so we can free it later. */
  str_beg = str;

  if (base == 0) {
    /* Skip whitespace. */
    while (*str && isspace(*str))
      str++;

    /* Check if the number is negative. */
    if (*str == '-') {
      neg = 1;
      str++;

      /* Skip whitespace. */
      while (*str && isspace(*str))
	str++;
    }

    /* Get the base. */
    if (*str == '0') {
      str++;

      if (!*str || *str == decimal_point || strchr("ep@", *str)) {
	/* Even though it starts with '0', it is still decimal because
	   the next character is a decimal point, a power, or the end
	   of the string. Note that mpfr_strtofr() does not detect
	   octal and that mpf_set_str() does not detect any base at
	   all. However, I think this is the best solution. */
	base = 10;
      }
      else {
	switch (tolower(*str)) {
	case 'x':
	  base = 16;
	  str++;
	  break;
	case 'b':
	  base = 2;
	  str++;
	  break;
	default:
	  base = 8;
	  break;
	}
      }
    }
    else {
      base = 10;
    }
  }

  /* Get the exponent string (if any). */
  str_size = strlen(str);
  for (size_t i = 0; i < str_size; i++) {
    switch (tolower(str[i])) {
    case 'e':
      /* An 'e' exponent is only valid in bases up to 10. */
      if (base <= 10)
	exp_base = base;

      break;
    case 'p':
      /* A 'p' exponent is only valid in bases 2 and 16. */
      if (base == 2 || base == 16)
	exp_base = 2;

      break;
    case '@':
      /* An '@' exponent is always valid. */
      exp_base = base;

      break;
    }

    if (exp_base) {
      /* This string does not have the 'e'/'p'/'@'. */
      exp_str = str + i + 1;

      if (*exp_str == '-') {
	/* Remember that we have a negative exponent. */
	neg_exp = 1;
	exp_str++;

	if (*exp_str == '-' || *exp_str == '+') {
	  free(str_beg);
	  return -1;
	}
      }

      if (!*exp_str || isspace(*exp_str)) {
	free(str_beg);
	return -1;
      }

      /* Remove the exponent from `str'. */
      str[i] = '\0';

      break;
    }
  }

  /* Initialize the multiplier, defaulting to 1. */
  mpz_init_set_ui(multiplier, 1);

  if (exp_base) {
    mpz_t i, exp;

    /* Parse the exponent (for some reason, the convention is to always
       parse this as decimal). */
    if (mpz_init_set_str(exp, exp_str, 10)) {
      mpz_clear(exp);
      free(str_beg);
      return -1;
    }

    /* The exponent should be non-negative (if it's negative, it will
       actually be positive and `neg_exp' will be set to nonzero). */
    assert(mpz_cmp_ui(exp, 0) >= 0);

    /* `exp_base' must be positive. */
    assert(exp_base > 0);

    /* Raise `exp_base' to `exp'. There is no GMP function to do this,
       so we must do it manually. */
    mpz_set_ui(multiplier, exp_base);
    for (mpz_init_set_ui(i, 1); mpz_cmp(i, exp) < 0; mpz_add_ui(i, i, 1))
      mpz_mul_ui(multiplier, multiplier, exp_base);

    mpz_clears(exp, i, NULL);
  }

  /* Find the decimal point. */
  after_decimal_str = strchr(str, decimal_point);
  if (after_decimal_str) {
    /* Separate the part before the decimal point from the part after
       the decimal point. */
    *after_decimal_str = '\0';

    /* This is now the string after the decimal point. */
    after_decimal_str++;

    /* Skip whitespace. */
    while (*after_decimal_str && isspace(*after_decimal_str))
      after_decimal_str++;

    if (*after_decimal_str == '+' || *after_decimal_str == '-')
      return -1;

    /* Allow a trailing '\0'. Treat it as if there were no decimal
       point. */
    if (!*after_decimal_str)
      after_decimal_str = NULL;
  }

  /* Parse the part before the decimal. */
  if (mpz_init_set_str(before_decimal, str, base)) {
    mpz_clear(before_decimal);
    free(str_beg);
    return -1;
  }

  mpq_init(after_decimal);

  if (after_decimal_str) {
    const unsigned long after_decimal_size = strlen(after_decimal_str);
    size_t after_decimal_count = 0;

    /* Count non-whitespace characters. */
    for (unsigned long i = 0; i < after_decimal_size; i++) {
      if (!isspace(after_decimal_str[i]))
	after_decimal_count++;
    }

    /* Parse the numerator. */
    if (mpz_set_str(mpq_numref(after_decimal), after_decimal_str, base)) {
      mpz_clears(before_decimal, after_decimal, NULL);
      free(str_beg);
      return -1;
    }

    /* This should not be negative. */
    assert(mpz_cmp_ui(mpq_numref(after_decimal), 0) >= 0);

    /* Calculate the denominator. */
    mpz_ui_pow_ui(mpq_denref(after_decimal), base, after_decimal_count);

    mpq_canonicalize(after_decimal);
  }

  /* We are finally done with this. */
  free(str_beg);

  /* Convert `before_decimal' to a rational number so we can perform
     rational arithmetic on it. */
  mpq_set_z(res, before_decimal);

  /* We are done with this now. */
  mpz_clear(before_decimal);

  /* Perform the final calculations. */
  mpq_add(res, res, after_decimal);

  /* We don't need this anymore. */
  mpq_clear(after_decimal);

  if (neg_exp)
    mpz_mul(mpq_denref(res), mpq_denref(res), multiplier);
  else
    mpz_mul(mpq_numref(res), mpq_numref(res), multiplier);

  mpq_canonicalize(res);

  /* We are done with this. */
  mpz_clear(multiplier);

  /* Don't forget to negate it if we need to. */
  if (neg)
    mpq_neg(res, res);

  return 0;
}
#endif /* HAVE_LIBGMP_ALL */

/* Check if the square only has numbers between 1 and the size
   squared, and if it does, fill in the other numbers with the missing
   values. Returns zero on success, nonzero on error. */
static int fill_other_nums(square_t *square) {
  size_t max = square->size * square->size;
  cellvali_t raw_nums[max];
  size_t raw_nums_size;

  if (square->nums_size == 0) {
    /* We have nothing to do (assuming the caller set
       square->nums_size correctly). */
    return 0;
  }

  /* Initialize `raw_nums' with all the numbers between 1 and `max'
     inclusive. */
  for (raw_nums_size = 0; raw_nums_size < max; raw_nums_size++)
    cellvali_init_set_ui(raw_nums[raw_nums_size], raw_nums_size + 1);

  /* Remove the numbers that are already in the square */
  for (size_t x = 0; x < square->size; x++) {
    for (size_t y = 0; y < square->size; y++) {
      if (!square->cells[x][y].mutable) {
	char found = 0;

	if (!cellval_int_p(square->cells[x][y].val)) {
	  /* It must be an integer. */

#ifdef HAVE_LIBGMP_ALL
	  return 1;
#else /* !HAVE_LIBGMP_ALL */
	  if (square->cells[x][y].val.type == FLOAT)
	    return 1;

	  /* It's neither INT nor FLOAT. */
	  errno = EINVAL;
	  return -1;
#endif /* !HAVE_LIBGMP_ALL */
	}

	/* It can't be greater than `max' or less than one */
	if (cellval_lt_ui(square->cells[x][y].val, 1) ||
	    cellval_gt_ui(square->cells[x][y].val, max))
	  return 1;

	/* Remove the number */
	for (size_t i = 0; i < raw_nums_size; i++) {
	  if (cellval_equal_cellvali(square->cells[x][y].val, raw_nums[i])) {
	    /* Remove it */
	    cellvali_clear(raw_nums[i]);
	    raw_nums[i] = raw_nums[--raw_nums_size];
	    found = 1;
	    break;
	  }
	}

	if (!found) {
	  /* There was a duplicate number. That means we can't guess
	     the other numbers. */
	  return 1;
	}
      }
    }
  }

  if (raw_nums_size != square->nums_size) {
    /* This is a mistake on the part of the caller */
    errno = EINVAL;
    return -1;
  }

  square->nums =
    mu_xmalloc(sizeof(*(square->nums)) * square->nums_size);

  /* Copy over the numbers */
  for (size_t i = 0; i < square->nums_size; i++) {
    cellval_init(square->nums[i]);
    cellval_set_cellvali(square->nums[i], raw_nums[i]);
    cellvali_clear(raw_nums[i]);
  }

  return 0;
}
