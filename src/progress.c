/************************************************************************\
 * Magic Square solves magic squares.                                   *
 * Copyright (C) 2019, 2020  Asher Gordon <AsDaGo@posteo.net>           *
 *                                                                      *
 * This file is part of Magic Square.                                   *
 *                                                                      *
 * Magic Square is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * Magic Square is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with Magic Square.  If not, see                                *
 * <https://www.gnu.org/licenses/>.                                     *
\************************************************************************/

/* progress.c -- functions for displaying progress bars */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>
#ifdef HAVE_LIBGMP_PROGRESS
# include <gmp.h>
#endif

#include "progress.h"

#ifdef HAVE_LIBGMP_PROGRESS
# define GET_FRAC_DONE(res, num) do {		\
    mpz_set(mpq_numref(frac_done_rat), num);	\
    (res) = mpq_get_d(frac_done_rat);		\
  } while (0)
#else /* !HAVE_LIBGMP_PROGRESS */
# define GET_FRAC_DONE(res, num)		\
  ((res) = (double)(num) / (double)progress_max)
#endif /* !HAVE_LIBGMP_PROGRESS */

static progress_mode_t progress_mode;
static progress_t progress_max;
static FILE *progress_file;
static const char *progress_filename;
static double prev_dot_progress;
static char write_error;
static unsigned short term_width;
static struct sigaction old_action;
#ifdef HAVE_LIBGMP_PROGRESS
static mpq_t frac_done_rat;
#endif
static char progress_initialized = 0;

/* Static helper functions */
int display_progress_bar(double);
int get_term_width(void);
void winch_handler(int);

/* Parse a string representation of a mode, returning the mode. */
progress_mode_t progress_parse_mode(const char *mode_str) {
  size_t mode_str_size = strlen(mode_str);

  if (!strncasecmp(mode_str, "auto", mode_str_size))
    return AUTO;
  if (!strncasecmp(mode_str, "bar", mode_str_size))
    return BAR;
  if (!strncasecmp(mode_str, "dot", mode_str_size))
    return DOT;

  return ERROR;
}

/* Initialize progress reporting. Returns zero on success, nonzero if
   we are already initialized (in which case nothing will happen). */
int progress_begin(progress_mode_t mode, progress_t max,
		   FILE *file, const char *filename) {
  if (progress_initialized)
    return 1;

  progress_mode		= mode;
  progress_init_set(progress_max, max);
  progress_file		= file;
  progress_filename	= filename;
  prev_dot_progress	= 0;

  write_error		= 0;

  if (progress_mode == BAR) {
    struct sigaction action;

#ifdef HAVE_LIBGMP_PROGRESS
    /* Initialize the GMP object to store the rational fraction done
       (which will be converted to a double for faster arithmetic and
       because we really don't need that much precision for this). */
    mpq_init(frac_done_rat);

    /* The denominator will always be `progress_max'. */
    mpz_set(mpq_denref(frac_done_rat), progress_max);
    /* Also set the numerator to `progress_max' (temporarily) so the
       automatic reallocation is done now to avoid repeated
       reallocations in the future. */
    mpz_set(mpq_numref(frac_done_rat), progress_max);
#endif /* HAVE_LIBGMP_PROGRESS */

    /* Get the terminal width. */
    get_term_width();

    /* Set up the signal handler for SIGWINCH. */
    action.sa_handler = winch_handler;
    sigemptyset(&(action.sa_mask));
    action.sa_flags = 0;

    if (sigaction(SIGWINCH, &action, &old_action)) {
      fprintf(stderr,
	      "%s: unable to trap SIGWINCH: %m\nresizing may not work\n",
	      program_invocation_name);
    }
  }

  progress_initialized = 1;

  return 0;
}

/* Display the progress indicator. Returns zero on success, nonzero on
   error (in which case a message will be written to stderr). */
int progress_display(progress_t num) {
  double frac_done;

  assert(progress_initialized);

  /* How much we have done. */
  GET_FRAC_DONE(frac_done, num);

  switch (progress_mode) {
  case BAR:
    if (display_progress_bar(frac_done))
      goto error;

    break;
  case DOT:
    while (frac_done - prev_dot_progress >= 0.001) {
      if (putc('.', progress_file) == EOF || fflush(progress_file) == EOF)
	goto error;

      prev_dot_progress += 0.001;
    }

    break;
  default:
    /* This should not happen. */
    assert(0);
  }

  /* We successfully wrote to `progress_file', so even if there was a
     previous write error, we want to take note of another one (since
     this write succeeded). */
  write_error = 0;

  return 0;

 error:
  if (!write_error) {
    fprintf(stderr, "%s: cannot write to %s: %m\n",
	    program_invocation_name, progress_filename);

    /* Remember that there was a write error so we don't keep
       outputing the message over and over again. */
    write_error = 1;
  }

  return 1;
}

/* Uninitialize progress reporting. Returns zero on success, nonzero
   on error. */
int progress_cancel(void) {
  assert(progress_initialized);

  progress_initialized = 0;

  progress_clear(progress_max);

  if (progress_mode == BAR) {
#ifdef HAVE_LIBGMP_PROGRESS
    mpq_clear(frac_done_rat);
#endif

    /* Reset the SIGWINCH handler. */
    if (sigaction(SIGWINCH, &old_action, NULL)) {
      fprintf(stderr, "%s: unable to reset SIGWINCH action: %m\n",
	      program_invocation_name);
      return 1;
    }
  }

  return 0;
}

/* Print the last progress bar and uninitialize progress
   reporting. Returns zero on success, nonzero on error. */
int progress_end(void) {
  int ret, ret1;

  assert(progress_initialized);

  ret = progress_display(progress_max);

  /* Print a final newline. */
  ret1 = putc('\n', progress_file);

  progress_cancel();

  return ret ? ret : ret1;
}

/***************************\
|* Static helper functions *|
\***************************/

/* Display a progress bar. Returns zero on success, nonzero on error,
   in which case `errno' will be set to indicate the error. */
int display_progress_bar(double frac_done) {
  unsigned short filled;
  unsigned short bar_max;

  assert(progress_initialized);

  if (term_width < 8) {
    errno = EMSGSIZE;
    return 1;
  }

  /* How big the progress bar should be (not counting the '[' and
     ']'). */
  bar_max = term_width - 7;

  /* And this is how much of the progress bar is filled. */
  filled = frac_done * bar_max;

  /* Reset the cursor and print the first '['. */
  if (fputs("\r[", progress_file) == EOF)
    return 1;

  /* Now print a line of '='s for how much we've done, except make the
     last character a '>' unless frac_done == 1. */
  for (unsigned short i = 1; i < filled; i++) {
    if (putc('=', progress_file) == EOF)
      return 1;
  }

  if (filled) {
    if (putc((frac_done == 1) ? '=' : '>', progress_file) == EOF)
      return 1;
  }

  /* Fill the rest in with spaces. */
  for (unsigned short i = filled; i < bar_max; i++) {
    if (putc(' ', progress_file) == EOF)
      return 1;
  }

  /* Print the percentage done. */
  if (fprintf(progress_file, "] %3.0f%%", frac_done * 100) < 0)
    return 1;

  if (fflush(progress_file) == EOF)
    return 1;

  return 0;
}

/* Get the terminal width and store it in the `term_width' global
   variable. Returns zero on success, nonzero on error (and prints an
   error message). `term_width' is guaranteed to be set to a
   meaningful value after this call even if an error is
   encountered. */
int get_term_width(void) {
  struct winsize size;
  int ret;

  ret = fileno(progress_file);

  /* Get the terminal size. */
  if (ret >= 0)
    ret = ioctl(ret, TIOCGWINSZ, &size);

  if (ret) {
    /* Assume that we have an 80 column terminal. */
    term_width = 80;

    /* Print an error message. */
    fprintf(stderr,
	    "%s: warning: cannot get terminal width: %m\nassuming %d columns",
	    program_invocation_name, term_width);
  }
  else {
    /* Store the width. */
    term_width = size.ws_col;
  }

  return ret;
}

/* Handle the SIGWINCH signal. */
void winch_handler(int signal) {
  assert(signal == SIGWINCH);
  get_term_width();
}
